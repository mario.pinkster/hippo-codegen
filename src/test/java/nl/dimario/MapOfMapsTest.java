package nl.dimario;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class MapOfMapsTest {

    private static String STRUCTAB = "{ \"a\":\"AAA\",  \"b\":\"BBB\"  }";
    private static String STRUCTCDE = "{ \"c\":\"CCC\",  \"d\":12, \"e\": true  }";
    private static String ARRAYFGH = "{ \"fgh\": [ \"FFF\", \"GGG\", \"HHH\" ]}";
    private static String ARRAYIJK = "{ \"ijk\": [ \"III\", 23, false ]}";

    private Map<String, Object> makeMom(String sjon) throws IOException {
        ObjectMapper om = new ObjectMapper();
        JsonNode json = om.readTree(sjon);
        MapOfMaps mom = new MapOfMaps(json);
        Map<String, Object> result = mom.getRootObject();
        return result;
    }

    private void checkMapAB(Map<String, Object> map, int size) {
        assertEquals("bad size", size, map.size());
        assertEquals("bad content", "AAA", map.get("a"));
        assertEquals("bad content", "BBB", map.get("b"));
    }

    private void checkMapCDE(Map<String, Object> map, int size) {
        assertEquals("bad size", size, map.size());
        assertEquals("bad content", 12, map.get("d"));
        assertEquals("bad content", "CCC", map.get("c"));
        assertEquals("bad content", true, map.get("e"));
    }

    private void checkListFGH( List<Object> list, int size) {
        assertEquals("bad size", size, list.size());
        assertEquals("bad content", "FFF", list.get(0));
        assertEquals("bad content", "GGG", list.get(1));
        assertEquals("bad content", "HHH", list.get(2));

    }

    private void checkListIJK( List<Object> list, int size) {
        assertEquals("bad size", size, list.size());
        assertEquals("bad content", "III", list.get(0));
        assertEquals("bad content", 23, list.get(1));
        assertEquals("bad content", false, list.get(2));
    }

    @Test
    public void testSimpleStruct() {
        try {
            Map<String, Object> map = makeMom(STRUCTAB);
            checkMapAB( map, 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStructWithValues() {
        try {
            Map<String, Object> map = makeMom(STRUCTCDE);
            checkMapCDE( map, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleArray() {
        try {
            Map<String, Object> map = makeMom(ARRAYFGH);
            List<Object> list = (List<Object>) map.get("fgh");
            checkListFGH( list, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testArrayWithValues() {
        try {
            Map<String, Object> map = makeMom(ARRAYIJK);
            List<Object> list = (List<Object>) map.get("ijk");
            checkListIJK( list, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testArrayInStruct() {

        try {
            ObjectMapper om = new ObjectMapper();
            JsonNode struct = om.readTree(STRUCTCDE);
            JsonNode array = om.readTree(ARRAYIJK).get("ijk");
            ((ObjectNode) struct).set("ijk", array);
            MapOfMaps mom = new MapOfMaps(struct);

            Map<String, Object> map = mom.getRootObject();
            checkMapCDE( map, 4);

            List<Object> list = (List<Object>) map.get("ijk");
            checkListIJK( list, 3);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStructInArray() {

        try {
            ObjectMapper om = new ObjectMapper();
            JsonNode arraywrap = om.readTree(ARRAYIJK);
            JsonNode array = arraywrap.get("ijk");
            JsonNode struct = om.readTree(STRUCTCDE);
            ((ArrayNode) array).set(1, struct);
            MapOfMaps mom = new MapOfMaps(arraywrap);

            Map<String, Object> map = mom.getRootObject();


            List<Object> list = (List<Object>) map.get("ijk");
            assertEquals("bad size", 3, list.size());

            Map<String, Object> mapstruct = (Map<String, Object>) list.get(1);
            checkMapCDE( mapstruct, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testArrayInStructInArrayInStruct() {

        try {
            ObjectMapper om = new ObjectMapper();
            JsonNode struct = om.readTree(STRUCTCDE);
            JsonNode array = om.readTree(ARRAYIJK).get("ijk");
            JsonNode substruct = om.readTree(STRUCTAB);
            JsonNode subarray = om.readTree(ARRAYFGH).get("fgh");

            ((ObjectNode) substruct).set("fgh", subarray);
            ((ArrayNode) array).add(substruct);
            ((ObjectNode) struct).set("ijk", array);

            MapOfMaps mom = new MapOfMaps(struct);

            Map<String, Object> map = mom.getRootObject();
            checkMapCDE( map, 4);

            List<Object> list = (List<Object>) map.get("ijk");
            assertEquals("bad size", 4, list.size());

            Map<String, Object> submap = (Map<String, Object>) list.get(3);
            checkMapAB( submap, 3);

            List<Object> sublist = (List<Object>) submap.get( "fgh");
            checkListFGH( sublist, 3);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}