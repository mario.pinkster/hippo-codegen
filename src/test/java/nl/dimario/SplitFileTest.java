package nl.dimario;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.junit.Test;

public class SplitFileTest {

    @Test
    public void testMinLeadingSpace() {
        try {
            SplitFile sf = new SplitFile( null, "    $$$file=test.yaml" );
            sf.append("     /nodus:");
            sf.append("      7890");
            sf.append("       890");
            sf.append("     67890");
            int min = sf.determineMinLeadingSpace();
            assertEquals("determineMinLeadingSpace()", 5, min);
            sf.append("  34567890");
            min = sf.determineMinLeadingSpace();
            assertEquals("determineMinLeadingSpace()", 2, min);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInit() {
        try {
            SplitFile sf = new SplitFile((SplitFile) null, "   $$$file=zout.yaml");
            sf.append("   hst:nodus:");
            assertEquals("init(): filename", "zout.yaml", sf.filename);
            assertEquals( "init(): node", "/hst:nodus", sf.nodepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSetDefConHeader() {

        try {
            assertFalse("Default value for global setting \"SplitFile.writeDefConHeader\" is incorrect", SplitFile.globalWriteDefConHeader);

            SplitFile.setGlobal("    $$$writeDefConHeader=true");
            assertTrue("SplitFile.setGlobal() doesn't change the value of global setting for \"SplitFile.writeDefConHeader\" to true",
                    SplitFile.globalWriteDefConHeader);

            SplitFile.setGlobal("    $$$writeDefConHeader=false");
            assertFalse("SplitFile.setGlobal() doesn't change the value of global setting for \"SplitFile.writeDefConHeader\" to false",
                    SplitFile.globalWriteDefConHeader);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testCurrentFilePath() {

        try {
            SplitFile.setOutputPath( "/tmp");
            SplitFile.setGlobal( "   $$$path=aaa");
            SplitFile aaa = new SplitFile( null,  "$$$file=aaa.yaml");
            aaa.append( "add dummy line to run init()");
            SplitFile.setGlobal( "    $$$path=aaa/bbb");
            SplitFile bbb = new SplitFile( null, "$$$file=bbb.yaml");
            bbb.append( "add dummy line to run init()");

            assertEquals( "Wrong filepath", "/tmp/aaa", aaa.getFilePath());
            assertEquals( "Wrong filepath", "/tmp/aaa/bbb", bbb.getFilePath());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testNodePath() {

        try {
            SplitFile.setGlobal( "     $$$rootNode=/content/documents/aaa");
            SplitFile aaa = new SplitFile( null,  "$$$file=aaa.yaml");
            aaa.append( "  /aaa:");
            aaa.append( "dummy line");

            SplitFile.setGlobal( "     $$$rootNode=/content/documents/bbb");
            SplitFile bbb = new SplitFile( null,  "$$$file=bbb.yaml");
            bbb.append( "bbb:");
            bbb.append( "dummy line");

            assertEquals( "Wrong nodepath", "/content/documents/aaa/aaa", aaa.nodepath);
            assertEquals( "Wrong nodepath", "/content/documents/bbb/bbb", bbb.nodepath);

            // reset for other tests
            SplitFile.setGlobal( "     $$$rootNode=/");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

            @Test
    public void testPrepareNoHeadersRemoveIndent() {
        try {
            SplitFile sf = new SplitFile(null, "  $$$file=test.yaml");
            String NINESPACE = "         ";
            sf.append( NINESPACE + "/nodus:");
            sf.append( NINESPACE + "  890");
            sf.append( NINESPACE + "    0");
            sf.append( NINESPACE + "  67890");
            String test = sf.prepareYamlContent();

            String[] ar = test.split( System.lineSeparator());
            assertEquals( "Wrong number of lines in output", 4, ar.length);
            assertEquals( "prepareContent(), no header, remove indent", "/nodus:",ar[0]);
            assertEquals( "prepareContent(), no header, remove indent", "  890",  ar[1]);
            assertEquals( "prepareContent(), no header, remove indent", "    0",  ar[2]);
            assertEquals( "prepareContent(), no header, remove indent", "  67890",  ar[3]);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testPrepareYesHeadersRemoveIndent() {
        try {
            SplitFile.globalWriteDefConHeader = true;
            SplitFile sf = new SplitFile(null, "$$$file=test.out");
            String NINESPACE = "         ";
            sf.append( NINESPACE + "/nodus:");
            sf.append( NINESPACE + "  890");
            sf.append( NINESPACE + "    0");
            sf.append( NINESPACE + "67890");
            String test = sf.prepareYamlContent();

            String[] ar = test.split( System.lineSeparator());
            assertEquals( "Wrong number of lines in output", 6, ar.length);
            assertEquals( "prepareContent() adds bad \"definitions:\" header", "definitions:", ar[0]);
            assertEquals( "prepareContent() adds bad \"  config:\" header", "  config:", ar[1]);
            assertEquals( "prepareContent(), header, remove indent, node", "    /nodus:", ar[2]);
            assertEquals( "prepareContent(), header, remove indent", "      890", ar[3]);
            assertEquals( "prepareContent(), header, remove indent", "        0", ar[4]);
            assertEquals( "prepareContent(), header, remove indent", "    67890", ar[5]);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testPrepareYesHeadersAddIndent() {
        try {
            SplitFile.globalWriteDefConHeader = true;
            SplitFile sf = new SplitFile(null, "$$$file=test.out");
            String TWOSPACE = "  ";
            sf.append( TWOSPACE + "  /nodus:");
            sf.append( TWOSPACE + "  890");
            sf.append( TWOSPACE + "    0");
            sf.append( TWOSPACE + "67890");
            String test = sf.prepareYamlContent();

            String[] ar = test.split( System.lineSeparator());
            assertEquals( "Wrong number of lines in output", 6, ar.length);
            assertEquals( "prepareContent() adds bad \"definitions:\" header", "definitions:", ar[0]);
            assertEquals( "prepareContent() adds bad \"  config:\" header", "  config:", ar[1]);
            assertEquals( "prepareContent(), no header, remove indent, node", "    /nodus:", ar[2]);
            assertEquals( "prepareContent(), no header, remove indent", "      890", ar[3]);
            assertEquals( "prepareContent(), no header, remove indent", "        0", ar[4]);
            assertEquals( "prepareContent(), no header, remove indent", "    67890", ar[5]);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

//    @Test
//    public void testGenerateScript() {
//
//        SplitFile sf = new SplitFile( null, "$$$file=script.groovy.ftl");
//    }
}