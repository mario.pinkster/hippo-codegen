$$$writeDefConHeader=true
$$$path=hcm-config/platform/hosts
$$$rootNode=/hst:platform/hst:hosts
$$$file=cms-${hostGroup}.yaml
/${hostGroup}:
  jcr:primaryType: hst:virtualhostgroup
  hst:defaultport: 443
  /eu:
     jcr:primaryType: hst:virtualhost
     hst:scheme: https
     hst:showcontextpath: false
     hst:showport: false
     hst:schemenotmatchresponsecode: 301
     /triodos:
       jcr:primaryType: hst:virtualhost
       /cms-${hostGroup}:
         jcr:primaryType: hst:virtualhost
         /hst:root:
           jcr:primaryType: hst:mount
           hst:ismapped: false
           hst:namedpipeline: WebApplicationInvokingPipeline
