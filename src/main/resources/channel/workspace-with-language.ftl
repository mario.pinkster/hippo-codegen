<#list languages as language>
$$$writeDefConHeader=true
$$$path=hcm-config/hst/configurations/${language.hstAlias}
$$$rootNode=/hst:hst/hst:configurations/${language.hstAlias}
$$$file=workspace.yaml
/hst:workspace:
  jcr:primaryType: hst:workspace
  $$$path=hcm-config/hst/configurations/${language.hstAlias}/workspace
  $$$rootNode=/hst:hst/hst:configurations/${language.hstAlias}/hst:workspace
  $$$file=channel.yaml
  /hst:channel:
    jcr:primaryType: hst:channel
    hst:channelinfoclass: com.triodos.site.info.SiteInfo
    hst:name: ${language.channelDisplayName}
    hst:type: website
    /hst:channelinfo:
      jcr:primaryType: hst:channelinfo
  $$$file=containers.yaml
  /hst:containers:
    jcr:primaryType: hst:containercomponentfolder
    /article-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /article-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ArticleStripComponent
          hst:iconpath: resources/article-strip-component.png
          hst:label: Article Strip
          hst:parameternames: [article, com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off', 'off']
          hst:template: article-strip-component-placeholder.ftl
          hst:xtype: hst.item
    /press-release-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /article-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ArticleStripComponent
          hst:iconpath: resources/article-strip-component.png
          hst:label: Article Strip
          hst:parameternames: [article, com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off', 'off']
          hst:template: article-strip-component-placeholder.ftl
          hst:xtype: hst.item
    /homepage:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    <#if language.splashPageHere??>
    /error-page-root:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    </#if>
    /error-page-404:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /error-page-500:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /project-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /project-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ProjectStripComponent
          hst:iconpath: resources/project-strip-component.png
          hst:label: Project Strip
          hst:parameternames: [com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off']
          hst:template: project-strip-component.ftl
          hst:xtype: hst.item
    /faq-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /verint-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.VerintStripComponent
          hst:iconpath: resources/verint-strip-component.png
          hst:label: Verint Strip
          hst:template: verint-strip-component.ftl
          hst:xtype: hst.item
    /vacancy-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /how-to-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /verint-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.VerintStripComponent
          hst:iconpath: resources/verint-strip-component.png
          hst:label: Verint Strip
          hst:parameternames: [org.hippoecm.hst.core.component.template]
          hst:parametervalues: ['webfile:/freemarker/catalog/verint-strip-component/verint-strip-component-how-to.ftl']
          hst:template: verint-strip-component.ftl
          hst:xtype: hst.item
    <#if language.splashPageHere??>
    /splashpage:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    </#if>
  $$$file=pages.yaml
  /hst:pages:
    jcr:primaryType: hst:pages
    <#if fundPage>
    /${language.sitemapTranslation.funds}-generic-page-prototype:
      jcr:primaryType: hst:component
      hst:referencecomponent: hst:abstractpages/base
      /main:
        jcr:primaryType: hst:component
        /container:
          jcr:primaryType: hst:containercomponent
          hst:xtype: hst.nomarkup
    </#if>
  <#if language.splashPageHere??>
  $$$path=hcm-config/hst/configurations/${language.hstAlias}/pages
  $$$rootNode=/hst:hst/hst:configurations/${language.hstAlias}/hst:pages
  $$$file=splashpage.yaml
  /splashpage:
      jcr:primaryType: hst:component
      hst:referencecomponent: hst:abstractpages/base-empty
      /main:
        jcr:primaryType: hst:component
        /container:
          jcr:primaryType: hst:containercomponentreference
          hst:referencecomponent: splashpage/container
  $$$path=hcm-config/hst/configurations/${language.hstAlias}/workspace
  $$$rootNode=/hst:hst/hst:configurations/${language.hstAlias}/hst:workspace
  </#if>
  $$$file=sitemap.yaml
  /hst:sitemap:
    jcr:primaryType: hst:sitemap
    <#if fundPage>
    /${language.sitemapTranslation.funds}:
      jcr:primaryType: hst:sitemapitem
      hst:refId: funds
      hst:componentconfigurationid: hst:pages/${language.sitemapTranslation.funds}-generic-page-prototype
      hst:relativecontentpath: ${documentFolderName}/${language.languageCode}/funds/funds
    </#if>
  $$$file=sitemenus.yaml
  /hst:sitemenus:
    jcr:primaryType: hst:sitemenus
    /branch-site-selector:
      jcr:primaryType: hst:sitemenu
    /language-selector:
      jcr:primaryType: hst:sitemenu
      /hst:prototypeitem:
        jcr:primaryType: hst:sitemenuitem
        hst:parameternames: [locale]
        hst:parametervalues: ['Please specify the locale for example: fr_BE_tbbe0']
    /top-menu:
      jcr:primaryType: hst:sitemenu
    /footer-links:
      jcr:primaryType: hst:sitemenu
    /social-media-links:
      jcr:primaryType: hst:sitemenu
      /hst:prototypeitem:
        jcr:primaryType: hst:sitemenuitem
        hst:parameternames: [icon]
        hst:parametervalues: ['PICK ONE: facebook, twitter, linkedin, youtube or instagram']
</#list>