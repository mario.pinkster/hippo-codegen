$$$writeDefConHeader=true
$$$path=hcm-config/hst/hosts
$$$rootNode=/hst:hst/hst:hosts/${hostGroup}/eu/triodos
$$$file=${virtualHostName}.yaml
/${virtualHostName}:
  .meta:residual-child-node-category: content
  jcr:primaryType: hst:virtualhost
  /hst:root:
    .meta:residual-child-node-category: content
    jcr:primaryType: hst:mount
  <#if virtualHostName?contains("stg")>
    hst:type: preview
  </#if>
    hst:parameternames: ['searchScopePattern']
    hst:parametervalues: ['${documentFolderName} | shared | shared-other']
    hst:mountpoint: /hst:hst/hst:sites/${hstAlias}
    hst:homepage: root
    hst:locale: ${locale}
    hst:alias: ${hstAlias}
    /restservices:
      .meta:residual-child-node-category: content
      jcr:primaryType: hst:mount
      hst:alias: restservices-${hstAlias}
      hst:ismapped: false
      hst:namedpipeline: JaxrsRestPlainPipeline
      hst:nochannelinfo: true
      hst:types: [rest]
      <#if virtualHostName?contains("stg")>
      hst:type: preview
      </#if>
    /gdpr:
      .meta:residual-child-node-category: content
      jcr:primaryType: hst:mount
      hst:alias: gdpr-${hstAlias}
      hst:ismapped: false
      hst:namedpipeline: TargetingRestApiPipeline
      hst:nochannelinfo: true
      hst:types: [rest]
