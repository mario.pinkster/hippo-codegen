$$$writeDefConHeader=true
$$$path=hcm-config/hst/configurations/
$$$rootNode=/hst:hst/hst:configurations/
$$$file=${hstRootAlias}.yaml
  /${hstRootAlias}:
    jcr:primaryType: hst:configuration
    hst:inheritsfrom: <#if inheritsConfigurationFrom?has_content>[${inheritsConfigurationFrom}] <#else>[../root]</#if>
<#list languages as language>
$$$writeDefConHeader=true
$$$path=hcm-config/hst/configurations/
$$$rootNode=/hst:hst/hst:configurations/
$$$file=${language.hstAlias}.yaml
/${language.hstAlias}:
  jcr:primaryType: hst:configuration
  hst:inheritsfrom: <#if inheritsConfigurationFrom?has_content>[${inheritsConfigurationFrom}] <#else>[../common, ../root]</#if>
  $$$path=hcm-config/hst/configurations/${language.hstAlias}
  $$$rootNode=/hst:hst/hst:configurations/${language.hstAlias}
  $$$file=abstractpages.yaml
  /hst:abstractpages:
    jcr:primaryType: hst:pages
  $$$file=catalog.yaml
  /hst:catalog:
    jcr:primaryType: hst:catalog
    /${language.hstAlias}-catalog:
      jcr:primaryType: hst:containeritempackage
  $$$file=components.yaml
  /hst:components:
    jcr:primaryType: hst:components
  $$$file=pages.yaml
  /hst:pages:
    jcr:primaryType: hst:pages
  $$$file=prototypepages.yaml
  /hst:prototypepages:
    jcr:primaryType: hst:pages
  $$$file=sitemenus.yaml
  /hst:sitemenus:
    jcr:primaryType: hst:sitemenus
  $$$file=templates.yaml
  /hst:templates:
    jcr:primaryType: hst:templates
  $$$file=sitemap.yaml
  /hst:sitemap:
    jcr:primaryType: hst:sitemap
    /root:
      jcr:primaryType: hst:sitemapitem
      hst:componentconfigurationid: hst:pages/homepage
      hst:refId: homepage
      hst:relativecontentpath: ${documentFolderName}/${language.languageCode}/homepage/homepage
    <#if language.splashPageHere??>
    /splashpage:
      jcr:primaryType: hst:sitemapitem
      hst:componentconfigurationid: hst:pages/splashpage
      hst:refId: splashpage
      hst:relativecontentpath: ${documentFolderName}/${language.languageCode}/splashpage/splashpage
    </#if>
    /${language.sitemapTranslation.articles}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      /_default_:
        jcr:primaryType: hst:sitemapitem
        /_default_:
          jcr:primaryType: hst:sitemapitem
          hst:componentconfigurationid: hst:pages/article-page
          hst:relativecontentpath: shared/articles/${language.languageCode}/<#noparse>${1}/${2}</#noparse>
          hst:sitemapitemhandlerids: [article-exists-site-map-item-handler, article-allowed-site-map-item-handler]
    /${language.sitemapTranslation.projects}:
      jcr:primaryType: hst:sitemapitem
      hst:refId: projects
      /_default_:
        jcr:primaryType: hst:sitemapitem
        /_default_:
          jcr:primaryType: hst:sitemapitem
          hst:componentconfigurationid: hst:pages/project-page
          hst:parameternames: [slug, locationID]
          hst:parametervalues: <#noparse>['${1}', '${2}']</#noparse>
          hst:sitemapitemhandlerids: [project-location-exists-site-map-item-handler]
    /${language.sitemapTranslation.pressRelease}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      /_default_:
        jcr:primaryType: hst:sitemapitem
        /_default_:
          jcr:primaryType: hst:sitemapitem
          hst:componentconfigurationid: hst:pages/press-release-page
          hst:relativecontentpath: shared/press-releases/${language.languageCode}/<#noparse>${1}/${2}</#noparse>
          hst:sitemapitemhandlerids: [article-exists-site-map-item-handler, article-allowed-site-map-item-handler]
    /${language.sitemapTranslation.faq}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      hst:refId: faq
      /_default_:
        jcr:primaryType: hst:sitemapitem
        hst:componentconfigurationid: hst:pages/faq-page
        hst:parameternames: [slug]
        hst:parametervalues: <#noparse>['${1}']</#noparse>
        hst:sitemapitemhandlerids: [verint-document-exists-site-map-item-handler]
    /${language.sitemapTranslation.howTo}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      hst:refId: how-to
      /_default_:
        jcr:primaryType: hst:sitemapitem
        hst:componentconfigurationid: hst:pages/how-to-page
        hst:parameternames: [slug]
        hst:parametervalues: <#noparse>['${1}']</#noparse>
        hst:sitemapitemhandlerids: [verint-document-exists-site-map-item-handler]
    /${language.sitemapTranslation.vacancies}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      hst:refId: vacancies
      /_default_:
        jcr:primaryType: hst:sitemapitem
        hst:componentconfigurationid: hst:pages/vacancy-page
        hst:parameternames: [id]
        hst:parametervalues: <#noparse>['${1}']</#noparse>
        hst:sitemapitemhandlerids: [talentfinder-vacancy-exists-site-map-item-handler]
        /_default_:
          jcr:primaryType: hst:sitemapitem
          hst:componentconfigurationid: hst:pages/vacancy-page
          hst:parameternames: [id, slug]
          hst:parametervalues: <#noparse>['${1}', '${2}']</#noparse>
          hst:sitemapitemhandlerids: [talentfinder-vacancy-exists-site-map-item-handler]
    /${language.sitemapTranslation.downloads}:
      jcr:primaryType: hst:sitemapitem
      hst:hiddeninchannelmanager: true
      hst:refId: downloads
      /_default_:
        jcr:primaryType: hst:sitemapitem
        hst:parameternames: [slug]
        hst:parametervalues: <#noparse>['${1}']</#noparse>
        hst:sitemapitemhandlerids: [verint-document-exists-site-map-item-handler, verint-download-site-map-item-handler]
</#list>