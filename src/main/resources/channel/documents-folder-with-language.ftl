$$$writeDefConHeader=false;
$$$path=hcm-content/content/documents/${branchName}
$$$rootNode=/content/documents/${branchName}
$$$file=${channelId}.yaml
/${channelId}:
  jcr:primaryType: hippostd:folder
  jcr:mixinTypes: ['hippo:named', 'mix:versionable']
  hippo:name: ${documentFolderName?capitalize}
  hippostd:foldertype: [new-folder]
  <#list languages as language>
  $$$path=hcm-content/content/documents/${branchName}/${channelId}
  $$$rootNode=/content/documents/${branchName}/${channelId}
  $$$file=${language.languageCode}.yaml
  /${language.languageCode}:
    jcr:primaryType: hippostd:folder
    jcr:mixinTypes: ['hippo:named', 'mix:versionable', 'hippotranslation:translated']
    hippo:name: ${language.languageCode}
    hippostd:foldertype: [new-translated-folder]
    hippotranslation:id: ${translationId[0]}
    hippotranslation:locale: ${language.locale}
    $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$file=homepage.yaml
    /homepage:
      jcr:primaryType: hippostd:folder
      jcr:mixinTypes: ['hippo:named', 'mix:versionable', 'hippotranslation:translated']
      hippo:name: Homepage
      hippostd:foldertype: [new-document, new-translated-folder]
      hippotranslation:id: ${translationId[1]}
      hippotranslation:locale: ${language.locale}
      $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}/homepage
      $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}/homepage
      $$$file=homepage.yaml
      /homepage:
        jcr:primaryType: hippo:handle
        jcr:mixinTypes: ['hippo:named', 'mix:referenceable']
        hippo:name: Homepage
        /homepage:
          jcr:primaryType: triodoshippo:pageDocument
          jcr:mixinTypes: ['hippotranslation:translated', 'mix:referenceable']
          hippo:availability: []
          hippostd:state: draft
          hippostdpubwf:createdBy: ${lastModifiedBy}
          hippostdpubwf:creationDate: ${lastModificationDate}
          hippostdpubwf:lastModificationDate: ${lastModificationDate}
          hippostdpubwf:lastModifiedBy: ${lastModifiedBy}
          hippotranslation:id: ${translationId[2]}
          hippotranslation:locale: ${language.locale}
          /triodoshippo:metadata:
            jcr:primaryType: triodoshippo:metadata
            triodoshippo:description: ''
            triodoshippo:title: ''
            triodoshippo:browserTitle: ''
            triodoshippo:ogDescription: ''
            triodoshippo:ogTitle: ''
            triodoshippo:keywords: ''
            triodoshippo:searchInternal: true
            triodoshippo:searchExternal: true
            /triodoshippo:ogImage:
              jcr:primaryType: hippogallerypicker:imagelink
              hippo:docbase: cafebabe-cafe-babe-cafe-babecafebabe
              hippo:facets: []
              hippo:modes: []
              hippo:values: []
    <#if fundPage>
    $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$file=funds.yaml
    /funds:
      jcr:primaryType: hippostd:folder
      jcr:mixinTypes: ['hippo:named', 'mix:versionable', 'hippotranslation:translated']
      hippo:name: ${language.sitemapTranslation.funds?capitalize}
      hippostd:foldertype: [new-document, new-translated-folder]
      hippotranslation:id: ${translationId[4]}
      hippotranslation:locale: ${language.locale}
      $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}/funds
      $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}/funds
      $$$file=funds.yaml
      /funds:
        jcr:primaryType: hippo:handle
        jcr:mixinTypes: ['hippo:named', 'mix:referenceable']
        hippo:name: Funds
        /funds:
          jcr:primaryType: triodoshippo:pageDocument
          jcr:mixinTypes: ['hippotranslation:translated', 'mix:referenceable']
          hippo:availability: []
          hippostd:state: draft
          hippostdpubwf:createdBy: ${lastModifiedBy}
          hippostdpubwf:creationDate: ${lastModificationDate}
          hippostdpubwf:lastModificationDate: ${lastModificationDate}
          hippostdpubwf:lastModifiedBy: ${lastModifiedBy}
          hippotranslation:id: ${translationId[5]}
          hippotranslation:locale: ${language.locale}
          /triodoshippo:metadata:
            jcr:primaryType: triodoshippo:metadata
            triodoshippo:description: ''
            triodoshippo:title: ''
            triodoshippo:browserTitle: ''
            triodoshippo:ogDescription: ''
            triodoshippo:ogTitle: ''
            triodoshippo:keywords: ''
            triodoshippo:searchInternal: true
            triodoshippo:searchExternal: true
            /triodoshippo:ogImage:
              jcr:primaryType: hippogallerypicker:imagelink
              hippo:docbase: cafebabe-cafe-babe-cafe-babecafebabe
              hippo:facets: []
              hippo:modes: []
              hippo:values: []
    </#if>
    <#if language.splashPageHere??>
    $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}
    $$$file=splashpage.yaml
    /splashpage:
      jcr:primaryType: hippostd:folder
      jcr:mixinTypes: ['hippo:named', 'mix:versionable', 'hippotranslation:translated']
      hippo:name: Splashpage
      hippostd:foldertype: [new-translated-folder, new-document]
      hippotranslation:id: ${translationId[6]}
      hippotranslation:locale: ${language.locale}
      $$$path=hcm-content/content/documents/${branchName}/${channelId}/${language.languageCode}/splashpage
      $$$rootNode=/content/documents/${branchName}/${channelId}/${language.languageCode}/splashpage
      $$$file=splashpage.yaml
      /splashpage:
        jcr:primaryType: hippo:handle
        jcr:mixinTypes: ['hippo:named', 'mix:referenceable']
        hippo:name: Splashpage
        /splashpage:
          jcr:primaryType: triodoshippo:pageDocument
          jcr:mixinTypes: ['hippotranslation:translated', 'mix:referenceable']
          hippo:availability: []
          hippostd:state: draft
          hippostdpubwf:createdBy: ${lastModifiedBy}
          hippostdpubwf:creationDate: ${lastModificationDate}
          hippostdpubwf:lastModificationDate: ${lastModificationDate}
          hippostdpubwf:lastModifiedBy: ${lastModifiedBy}
          hippotranslation:id: ${translationId[7]}
          hippotranslation:locale: ${language.locale}
          /triodoshippo:metadata:
            jcr:primaryType: triodoshippo:metadata
            triodoshippo:description: ''
            triodoshippo:title: ''
            triodoshippo:browserTitle: ''
            triodoshippo:ogDescription: ''
            triodoshippo:ogTitle: ''
            triodoshippo:keywords: ''
            triodoshippo:searchInternal: true
            triodoshippo:searchExternal: true
            /triodoshippo:ogImage:
              jcr:primaryType: hippogallerypicker:imagelink
              hippo:docbase: cafebabe-cafe-babe-cafe-babecafebabe
              hippo:facets: []
              hippo:modes: []
              hippo:values: []
    </#if>
  </#list>
