$$$path=hcm-config/configuration/update/registry
$$$file=AddChannelSelectionUpdater.groovy

package com.triodos.cms.admin.updater

import org.onehippo.repository.update.BaseNodeUpdateVisitor
import javax.jcr.Node
import javax.jcr.NodeIterator;

class AddChannelSelectionUpdater extends BaseNodeUpdateVisitor {

    boolean doUpdate(Node jcrRootNode) {
        Node branchDocumentsRootNode = jcrRootNode.getNode("content/documents/${branchName}");
        Node channelNode = branchDocumentsRootNode.getNode("system").getNode("channels").getNode("channels");
        boolean found = false;
        NodeIterator selectionNodeIterator = null;
        <#list languages as language>
        // NOTE : this code was generated from a template, that is why you see repetitions.
        selectionNodeIterator = channelNode.getNodes();
        found = false;
        while (selectionNodeIterator.hasNext()) {
            Node selectionItemNode = selectionNodeIterator.nextNode();
            if( "${language.hstAlias}".equals( selectionItemNode.getProperty("selection:key").getString())) {
               found = true;
               selectionItemNode.setProperty( "selection:label", "${language.channelDisplayName}");
               log.info( "Updated channel selection item in list: ${language.hstAlias}=${language.channelDisplayName}");
               break;
            }
        }
        if( !found) {
            Node newSelectionNode = channelNode.addNode("selection:listitem", "selection:listitem");
            newSelectionNode.setProperty("selection:key", "${language.hstAlias}");
            newSelectionNode.setProperty("selection:label", "${language.channelDisplayName}");
            log.info( "Added channel selection item to list: ${language.hstAlias}=${language.channelDisplayName}");
        }
        </#list>
        return true;
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
$$$writeDefConHeader=true;
$$$path=hcm-config/configuration/update/registry
$$$file=AddChannelSelectionUpdater.yaml
$$$rootNode=/hippo:configuration/hippo:update/hippo:registry
   /AddChannelSelectionUpdater:
     jcr:primaryType: hipposys:updaterinfo
     hipposys:batchsize: 10
     hipposys:description: ''
     hipposys:dryrun: false
     hipposys:parameters: ''
     hipposys:query: /jcr:root
     hipposys:script:
       type: string
       resource: ./AddChannelSelectionUpdater.groovy
     hipposys:throttle: 1000
   /hippo:configuration/hippo:update/hippo:queue/AddChannelSelectionUpdater-${updaterVersion}:
     jcr:primaryType: hipposys:updaterinfo
     hipposys:batchsize: 10
     hipposys:description: ''
     hipposys:dryrun: false
     hipposys:parameters: ''
     hipposys:query: /jcr:root
     hipposys:script:
       type: string
       resource: ./AddChannelSelectionUpdater.groovy
     hipposys:throttle: 1000
