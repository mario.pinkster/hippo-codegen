$$$path=hcm-config/configuration/update/registry
$$$file=AddChannelSelectionUpdater.groovy

package com.triodos.cms.admin.updater

import org.onehippo.repository.update.BaseNodeUpdateVisitor
import javax.jcr.Node
import javax.jcr.NodeIterator;

class AddChannelSelectionUpdater extends BaseNodeUpdateVisitor {

    boolean doUpdate(Node jcrRootNode) {
        Node branchDocumentsRootNode = jcrRootNode.getNode("content/documents/${branchName}");
        Node channelNode = branchDocumentsRootNode.getNode("system").getNode("channels").getNode("channels");
        NodeIterator selectionNodeIterator = channelNode.getNodes();
        boolean found = false;
        while (selectionNodeIterator.hasNext()) {
            Node selectionItemNode = selectionNodeIterator.nextNode();
            if( "${hstAlias}".equals( selectionItemNode.getProperty("selection:key").getString())) {
               found = true;
               selectionItemNode.setProperty( "selection:label", "${channelDisplayName}");
               log.info( "Updated channel selection item in list: ${hstAlias}=${channelDisplayName}");
               break;
            }
        }
        if( !found) {
            Node newSelectionNode = channelNode.addNode("selection:listitem", "selection:listitem");
            newSelectionNode.setProperty("selection:key", "${hstAlias}");
            newSelectionNode.setProperty("selection:label", "${channelDisplayName}");
            log.info( "Added channel selection item to list: ${hstAlias}=${channelDisplayName}");
        }
        return true;
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
$$$writeDefConHeader=true;
$$$path=hcm-config/configuration/update/registry
$$$file=AddChannelSelectionUpdater.yaml
$$$rootNode=/hippo:configuration/hippo:update/hippo:registry
   /AddChannelSelectionUpdater:
     jcr:primaryType: hipposys:updaterinfo
     hipposys:batchsize: 10
     hipposys:description: ''
     hipposys:dryrun: false
     hipposys:parameters: ''
     hipposys:query: /jcr:root
     hipposys:script:
       type: string
       resource: ./AddChannelSelectionUpdater.groovy
     hipposys:throttle: 1000
   /hippo:configuration/hippo:update/hippo:queue/AddChannelSelectionUpdater-${updaterVersion}:
     jcr:primaryType: hipposys:updaterinfo
     hipposys:batchsize: 10
     hipposys:description: ''
     hipposys:dryrun: false
     hipposys:parameters: ''
     hipposys:query: /jcr:root
     hipposys:script:
       type: string
       resource: ./AddChannelSelectionUpdater.groovy
     hipposys:throttle: 1000
