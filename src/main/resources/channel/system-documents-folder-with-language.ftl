$$$writeDefConHeader=false;
$$$path=hcm-content/content/documents/${branchName}/system
$$$rootNode=/content/documents/${branchName}/system
$$$file=${channelId}.yaml
/${channelId}:
  jcr:primaryType: hippostd:folder
  jcr:mixinTypes: ['hippo:named', 'mix:versionable']
  hippo:name: ${documentFolderName?capitalize}
  hippostd:foldertype: [new-document, new-folder]
