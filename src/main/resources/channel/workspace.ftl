$$$writeDefConHeader=true
$$$path=hcm-config/hst/configurations/${hstAlias}
$$$rootNode=/hst:hst/hst:configurations/${hstAlias}
$$$file=workspace.yaml
/hst:workspace:
  jcr:primaryType: hst:workspace
  $$$path=hcm-config/hst/configurations/${hstAlias}/workspace
  $$$rootNode=/hst:hst/hst:configurations/${hstAlias}/hst:workspace
  $$$file=channel.yaml
  /hst:channel:
    jcr:primaryType: hst:channel
    hst:channelinfoclass: com.triodos.site.info.SiteInfo
    hst:name: ${channelDisplayName}
    hst:type: website
    /hst:channelinfo:
      jcr:primaryType: hst:channelinfo
  $$$file=containers.yaml
  /hst:containers:
    jcr:primaryType: hst:containercomponentfolder
    /article-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /article-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ArticleStripComponent
          hst:iconpath: resources/article-strip-component.png
          hst:label: Article Strip
          hst:parameternames: [article, com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off', 'off']
          hst:template: article-strip-component-placeholder.ftl
          hst:xtype: hst.item
    /press-release-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /article-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ArticleStripComponent
          hst:iconpath: resources/article-strip-component.png
          hst:label: Article Strip
          hst:parameternames: [article, com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off', 'off']
          hst:template: article-strip-component-placeholder.ftl
          hst:xtype: hst.item
    /homepage:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /error-page-404:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /error-page-500:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /project-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /project-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.ProjectStripComponent
          hst:iconpath: resources/project-strip-component.png
          hst:label: Project Strip
          hst:parameternames: [com.onehippo.cms7.targeting.TargetingParameterUtil.hide,
            contextAwareDataProvider]
          hst:parametervalues: ['', 'off']
          hst:template: project-strip-component.ftl
          hst:xtype: hst.item
    /faq-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /verint-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.VerintStripComponent
          hst:iconpath: resources/verint-strip-component.png
          hst:label: Verint Strip
          hst:template: verint-strip-component.ftl
          hst:xtype: hst.item
    /vacancy-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
    /how-to-page:
      jcr:primaryType: hst:containercomponentfolder
      /container:
        jcr:primaryType: hst:containercomponent
        hst:xtype: hst.nomarkup
        /verint-strip-component:
          jcr:primaryType: hst:containeritemcomponent
          hst:componentclassname: com.triodos.site.components.VerintStripComponent
          hst:iconpath: resources/verint-strip-component.png
          hst:label: Verint Strip
          hst:parameternames: [org.hippoecm.hst.core.component.template]
          hst:parametervalues: ['webfile:/freemarker/catalog/verint-strip-component/verint-strip-component-how-to.ftl']
          hst:template: verint-strip-component.ftl
          hst:xtype: hst.item
  $$$file=pages.yaml
  /hst:pages:
    jcr:primaryType: hst:pages
    <#if fundPage>
    /${sitemapTranslation.funds}-generic-page-prototype:
      jcr:primaryType: hst:component
      hst:referencecomponent: hst:abstractpages/base
      /main:
        jcr:primaryType: hst:component
        /container:
          jcr:primaryType: hst:containercomponent
          hst:xtype: hst.nomarkup
    </#if>
  $$$file=sitemap.yaml
  /hst:sitemap:
    jcr:primaryType: hst:sitemap
    <#if fundPage>
    /${sitemapTranslation.funds}:
      jcr:primaryType: hst:sitemapitem
      hst:refId: funds
      hst:componentconfigurationid: hst:pages/${sitemapTranslation.funds}-generic-page-prototype
      hst:relativecontentpath: ${documentFolderName}/funds/funds
    </#if>
  $$$file=sitemenus.yaml
  /hst:sitemenus:
    jcr:primaryType: hst:sitemenus
    /branch-site-selector:
      jcr:primaryType: hst:sitemenu
    /language-selector:
      jcr:primaryType: hst:sitemenu
      /hst:prototypeitem:
        jcr:primaryType: hst:sitemenuitem
        hst:parameternames: [locale]
        hst:parametervalues: ['Please specify the locale for example: fr_BE_tbbe0']
    /top-menu:
      jcr:primaryType: hst:sitemenu
    /footer-links:
      jcr:primaryType: hst:sitemenu
    /social-media-links:
      jcr:primaryType: hst:sitemenu
      /hst:prototypeitem:
        jcr:primaryType: hst:sitemenuitem
        hst:parameternames: [icon]
        hst:parametervalues: ['PICK ONE: facebook, twitter, linkedin, youtube or instagram']