$$$writeDefConHeader=false
$$$path=hcm-content/hst/sites
$$$rootNode=/hst:hst/hst:sites
$$$file=${hstRootAlias}.yaml
  /${hstRootAlias}:
    jcr:primaryType: hst:site
    hst:content: /content/documents/${branchName}
<#list languages as language>
$$$file=${language.hstAlias}.yaml
  /${language.hstAlias}:
    jcr:primaryType: hst:site
    hst:content: /content/documents/${branchName}
</#list>