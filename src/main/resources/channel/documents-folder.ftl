$$$writeDefConHeader=false;
$$$path=hcm-content/content/documents/${branchName}
$$$rootNode=/content/documents/${branchName}
$$$file=app.yaml
/${channelId}:
  jcr:primaryType: hippostd:folder
  jcr:mixinTypes: ['hippo:named', 'mix:versionable']
  hippo:name: ${documentFolderName?capitalize}
  hippostd:foldertype: [new-folder]
  $$$path=hcm-content/content/documents/${branchName}/${channelId}
  $$$rootNode=/content/documents/${branchName}/${channelId}
  $$$file=homepage.yaml
  /homepage:
    jcr:primaryType: hippostd:folder
    jcr:mixinTypes: ['hippo:named', 'mix:versionable']
    hippo:name: Homepage
    hippostd:foldertype: [new-document, new-folder]
    $$$path=hcm-content/content/documents/${branchName}/${channelId}/homepage
    $$$rootNode=/content/documents/${branchName}/${channelId}/homepage
    $$$file=homepage.yaml
    /homepage:
      jcr:primaryType: hippo:handle
      jcr:mixinTypes: ['hippo:named', 'mix:referenceable', 'hippo:versionInfo']
      hippo:name: Homepage
      /homepage:
        hippotranslation:id: ${translationId[2]}
        hippotranslation:locale: 'document-type-locale'
        jcr:primaryType: triodoshippo:pageDocument
        jcr:mixinTypes: ['hippotranslation:translated', 'mix:referenceable']
        hippo:availability: []
        hippostd:state: draft
        hippostdpubwf:createdBy: ${lastModifiedBy}
        hippostdpubwf:creationDate: ${lastModificationDate}
        hippostdpubwf:lastModificationDate: ${lastModificationDate}
        hippostdpubwf:lastModifiedBy: ${lastModifiedBy}
        /triodoshippo:metadata:
          jcr:primaryType: triodoshippo:metadata
          triodoshippo:description: ''
          triodoshippo:title: ''
          triodoshippo:browserTitle: ''
          triodoshippo:ogDescription: ''
          triodoshippo:ogTitle: ''
          triodoshippo:keywords: ''
          triodoshippo:searchInternal: true
          triodoshippo:searchExternal: true
          /triodoshippo:ogImage:
            jcr:primaryType: hippogallerypicker:imagelink
            hippo:docbase: cafebabe-cafe-babe-cafe-babecafebabe
            hippo:facets: []
            hippo:modes: []
            hippo:values: []
  <#if fundPage>
    $$$path=hcm-content/content/documents/${branchName}/${channelId}
    $$$rootNode=/content/documents/${branchName}/${channelId}
    $$$file=funds.yaml
    /funds:
      jcr:primaryType: hippostd:folder
      jcr:mixinTypes: ['hippo:named', 'mix:versionable']
      hippo:name: ${sitemapTranslation.funds?capitalize}
      hippostd:foldertype: [new-document, new-folder]
      $$$path=hcm-content/content/documents/${branchName}/${channelId}/funds
      $$$rootNode=/content/documents/${branchName}/${channelId}/funds
      $$$file=funds.yaml
      /funds:
        jcr:primaryType: hippo:handle
        jcr:mixinTypes: ['hippo:named', 'mix:referenceable', 'hippo:versionInfo']
        hippo:name: Funds
        /funds:
          hippotranslation:id: ${translationId[5]}
          hippotranslation:locale: 'document-type-locale'
          jcr:primaryType: triodoshippo:pageDocument
          jcr:mixinTypes: ['hippotranslation:translated', 'mix:referenceable', 'hippotranslation:translated']
          hippo:availability: []
          hippostd:state: draft
          hippostdpubwf:createdBy: ${lastModifiedBy}
          hippostdpubwf:creationDate: ${lastModificationDate}
          hippostdpubwf:lastModificationDate: ${lastModificationDate}
          hippostdpubwf:lastModifiedBy: ${lastModifiedBy}
          /triodoshippo:metadata:
            jcr:primaryType: triodoshippo:metadata
            triodoshippo:description: ''
            triodoshippo:title: ''
            triodoshippo:browserTitle: ''
            triodoshippo:ogDescription: ''
            triodoshippo:ogTitle: ''
            triodoshippo:keywords: ''
            triodoshippo:searchInternal: true
            triodoshippo:searchExternal: true
            /triodoshippo:ogImage:
              jcr:primaryType: hippogallerypicker:imagelink
              hippo:docbase: cafebabe-cafe-babe-cafe-babecafebabe
              hippo:facets: []
              hippo:modes: []
              hippo:values: []
  </#if>