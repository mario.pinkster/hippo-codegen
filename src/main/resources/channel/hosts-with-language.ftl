$$$writeDefConHeader=true
$$$path=hcm-config/hst/hosts
$$$rootNode=/hst:hst/hst:hosts/${hostGroup}/eu/triodos
$$$file=${virtualHostName}.yaml
/${virtualHostName}:
  .meta:residual-child-node-category: content
  jcr:primaryType: hst:virtualhost
  /hst:root:
    .meta:residual-child-node-category: content
    jcr:primaryType: hst:mount
  <#if virtualHostName?contains("stg")>
    hst:type: preview
  </#if>
    hst:nochannelinfo: true
    hst:parameternames: ['searchScopePattern']
    hst:parametervalues: ['${documentFolderName} | shared | shared-other']
    hst:mountpoint: /hst:hst/hst:sites/${hstRootAlias}
    <#list languages as language>
    /${language.languageCode}:
      .meta:residual-child-node-category: content
      jcr:primaryType: hst:mount
      hst:mountpoint: /hst:hst/hst:sites/${language.hstAlias}
      hst:alias: ${language.hstAlias}
      hst:homepage: root
      hst:locale: ${language.locale}
      /restservices:
        .meta:residual-child-node-category: content
        jcr:primaryType: hst:mount
        hst:alias: restservices-${language.hstAlias}
        hst:ismapped: false
        hst:namedpipeline: JaxrsRestPlainPipeline
        hst:nochannelinfo: true
        hst:types: [rest]
        <#if virtualHostName?contains("stg")>
        hst:type: preview
        </#if>
      /gdpr:
        .meta:residual-child-node-category: content
        jcr:primaryType: hst:mount
        hst:alias: gdpr-${language.hstAlias}
        hst:ismapped: false
        hst:namedpipeline: TargetingRestApiPipeline
        hst:nochannelinfo: true
        hst:types: [rest]
    </#list>
