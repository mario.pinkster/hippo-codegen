package nl.dimario;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

/**
 * A DataSet reads all files in the given directory plus some file(s)
 * from the default data directory and makes them available to the
 * rest of the program.
 */
public class DataSet implements CodegenConstants {

    private List<String> hostGroups;

    private String dataDir;
    private String outputDir;

    public boolean init( String[] args) throws IOException {

        if( args.length != 1) {
            System.out.println("\n\n\nSyntax error in command line\n\n\n");
            System.out.println( "Usage: java -jar hippo-codegen-<version>.jar directoryName");
            System.out.println( "where directoryName points to a dir in /data/ that contains input data.");
            System.out.println( "Additional data files are located in /data/defaults/\n\n");
            return false;
        }
        this.dataDir = FilenameUtils.concat( DATA_DIR, args[0]);
        SplitFile.setOutputPath( this.getOutputDir( args[0]));
        return true;
    }

    public List<String> getHostgroups() throws IOException {
        if( hostGroups == null) {
            readHostgroups();
        }
        return this.hostGroups;
    }

    private File getHostGroupFile() throws IOException {
        File hgFile = new File( dataDir, HOSTGROUPS_FILE);
        if( ! hgFile.exists()) {
            hgFile = new File( DEFAULT_DATA_DIR, HOSTGROUPS_FILE);
        }
        if( ! hgFile.exists()) {
            throw new IOException( String.format( "Cannot find hostgroups.json in %s or in %s", this.dataDir, DEFAULT_DATA_DIR));
        }
        return hgFile;
    }

    private void readHostgroups() throws IOException {

        File input = getHostGroupFile();
        ObjectMapper om = new ObjectMapper();
        System.out.println("Getting HOSTGROUPS data from " + input.getAbsolutePath());
        String sinput = FileUtils.readFileToString(input, StandardCharsets.UTF_8);
        JsonNode data = om.readTree(sinput);
        ArrayNode arrayNode = (ArrayNode) data.get(HOSTGROUPS);
        List<String> list = new ArrayList<String>();

        for (int i = 0; i < arrayNode.size(); i++) {
            JsonNode valueNode = arrayNode.get(i);
            if (valueNode.getNodeType() == JsonNodeType.STRING) {
                list.add(valueNode.textValue());
            } else {
                throw new IOException(String.format( "Unexpected value type in file \"%s\". Should be JsonNodeType.STRING (%d) but is (%d)",
                        HOSTGROUPS_FILE, JsonNodeType.STRING, valueNode.getNodeType()));
            }
        }
        this.hostGroups = list;
    }

    public Map<String, Object> getRootData(String rootDataFileName) throws IOException {
        ObjectMapper om = new ObjectMapper();
        String dataFileName = FilenameUtils.concat( this.dataDir, rootDataFileName);
        File input = new File( dataFileName);
        System.out.println( "Getting input data from " + input.getAbsolutePath());
        String sinput = FileUtils.readFileToString( input, StandardCharsets.UTF_8);
        JsonNode data = om.readTree( sinput);
        MapOfMaps mom = new MapOfMaps( data);
        return mom.getRootObject();
    }

    public String getOutputDir( String dataPath) throws IOException {

        if( this.outputDir == null) {
            File generatorProperties = new File(DEFAULT_DATA_DIR, GENERATOR_CONFIG_FILE);
            FileReader reader = new FileReader(generatorProperties);
            Properties properties = new Properties();
            properties.load(reader);
            String basePath = properties.getProperty(OUTPUT_DIR, DEFAULT_OUTPUT_DIR);
            this.outputDir = FilenameUtils.concat( basePath, dataPath);
        }
        return this.outputDir;
    }
}
