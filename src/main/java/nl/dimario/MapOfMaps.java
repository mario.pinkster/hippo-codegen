package nl.dimario;

/**
 * This utility class converts a JsonNode structure to a structure
 * of Map, List and regular Objects which the Freemarker
 * engine can interpret without problems.
 * If you don't convert the input Json data object to a MapOfMaps,
 * the template engine can still find all replacement values but
 * every rendered value is surrounded by double quotes because
 * rendering Json objects as data is not supported properly.
 *
 * The alternative is to extend ObjectWrapper with your own custom
 * class to wrap JsonNode and ArrayNode into objects that
 * the freemarker engine does understand. That is a whole lot more
 * trouble than this relatively simple converter.
 *
 * After all, at the end of the day, everything gets printed as a String
 * and we don't need any fancy data conversions.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class MapOfMaps {

    private Map<String, Object> root;
    private JsonNode json;

    /**
     * The constructor merely stores the root JsonNode object of the
     * structured data that we want to convert.
     *
     * @param json
     */
    public MapOfMaps( JsonNode json) {
        this.json = json;
    }

    /**
     * This method performs the actual conversion
     * and returns the result.
     *
     * @return
     */
    public Map<String, Object> getRootObject() {

        if( this.root == null) {
            this.root = recurse( this.json);
        }
        return this.root;
    }

    /**
     * Here is the actual worker method for the conversion.
     * It turns one JsonNode into a Map of Objects. The
     * Objects in the Map may themselves be List, Map, or just ordinary
     * value objects such as String or Integer.
     *
     * The Map and List objects that are placed in the Map for one
     * JsonNode may themselves contain other Map and List objects,
     * thus effectively mimicking the hierarchy of JsonNode objects in
     * the JSON structure.
     *
     * When it is discovered that one of the objects in the input JsonNode
     * is a JsonNode or ArrayNode, a recursive call is made to process
     * the subobject in the same way.
     *
     * @param node
     * @return
     */
    private Map<String, Object> recurse( JsonNode node) {

        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<Map.Entry<String, JsonNode>> iter = node.fields();

        while (iter.hasNext()) {
            Map.Entry<String, JsonNode> item = iter.next();
            String key = item.getKey();
            JsonNode subNode = item.getValue();

            if (subNode.isValueNode()) {
                map.put(key, getValue(subNode));

            } else if( subNode.isObject()) {
                map.put( key, recurse( subNode));

            } else if (subNode.isArray()) {
                List<Object> list = new ArrayList<Object>();
                ArrayNode arrayNode = (ArrayNode) subNode;
                for( int i = 0; i< arrayNode.size(); i++) {
                    JsonNode arraySubNode = arrayNode.get( i);
                    if( arraySubNode.isObject() || arraySubNode.isArray()) {
                        list.add(recurse( arraySubNode));
                    } else {
                        list.add( getValue( arraySubNode));
                    }
                }
                map.put( key, list);
            }
        }
        return map;
    }

    /**
     * This method converts an ordinary value (id est, not a JsonNode and not an ArrayNode)
     * found in a subnode of the currently processed JsonNode to a normal
     * Java value that is stored in the Map for the current JsonNode or in the
     * List for the current ArrayNode.
     *
     * Presently, we are able to convert String, Integer and Boolean. Other types trigger
     * the UnsupportedOperationException.
     *
     *
     * @param valueNode
     * @return Normal String, Integer or Boolean object holding the value of the input node.
     * These are the objects that are used when replacing data in the template.
     * @throws UnsupportedOperationException
     */
    private Object getValue( JsonNode valueNode) {

        if( valueNode.getNodeType() ==  JsonNodeType.BOOLEAN) { return valueNode.asBoolean(); }
        if( valueNode.getNodeType() ==  JsonNodeType.STRING) { return valueNode.textValue(); }
        if( valueNode.getNodeType() ==  JsonNodeType.NUMBER) { return valueNode.asInt(); }

        throw new UnsupportedOperationException( "Cannot convert value");
    }
}
