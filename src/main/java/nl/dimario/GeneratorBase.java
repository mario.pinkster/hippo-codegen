package nl.dimario;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import nl.dimario.TemplateRenderer;

public abstract class GeneratorBase extends TemplateRenderer {

    private static final String WITH_LANGUAGE = "-with-language";

    protected static final String DOCUMENTS_FOLDER_TEMPLATE_NAME = "documents-folder";
    protected static final String HOSTS_TEMPLATE_NAME = "hosts";
    protected static final String SITES_TEMPLATE_NAME = "sites";
    protected static final String HST_CONFIGURATION_TEMPLATE_NAME = "hst-configuration";
    protected static final String SYSTEM_DOCUMENTS_FOLDER_TEMPLATE_NAME = "system-documents-folder";
    protected static final String WORKSPACE_TEMPLATE_NAME = "workspace";
    protected static final String UPDATE_CHANNEL_SELECTOR_SCRIPT_TEMPLATE_NAME = "update-channel-selector-script";

    protected static final String CHANNEL_DATA_FILENAME = "channel.json";
    protected static final String BRANCH_NAME ="branchName";
    protected static final String BRANCH_ID ="branchId";
    protected static final String CHANNEL_DISPLAY_NAME ="channelDisplayName";
    protected static final String CHANNEL_ID ="channelId";
    protected static final String COUNTRY_CODE = "countryCode";
    protected static final String DISPLAY_NAME_PART = "displayNamePart";
    protected static final String HOST_GROUP  = "hostGroup";
    protected static final String HST_ALIAS ="hstAlias";
    protected static final String HST_ROOT_ALIAS ="hstRootAlias";
    protected static final String LANGUAGE_CODE = "languageCode";
    protected static final String LANGUAGES = "languages";
    protected static final String LAST_MODIFIED_BY = "lastModifiedBy";
    protected static final String LAST_MODIFICATION_DATE = "lastModificationDate";
    protected static final String TRANSLATION_ID = "translationId";
    protected static final String VIRTUAL_HOST_NAME= "virtualHostName";

    private List<String> hostgroups;

    /**
     * Implementors must override this and return true if they want to use the language sensitive versions of FTL templates.
     *
     * @return if return true they want language sensitive versions of FTL templates.
     */
    protected  boolean isWithLanguages() {
        return this.rootObject.containsKey( LANGUAGES);
    }

    /**
     * Implementations must return their own subdir name under resources where the ftl templates are kept.
     */
    protected abstract String getTemplatePath();


    /**
     * Creates a template file name relative to the basepath that is configured for the template system in
     * FreemarkerConfiguration.setDirectoryForTemplateLoading.
     * We take the base filename, prepend the template directory, optionally add a "with languages"
     * suffix, and add the *.ftl extension.
     *
     * If isWithLanguages() is overridden to return true, the basename will get the {@value WITH_LANGUAGE} suffix.
     *
     * @param baseName the basic file name for the template w/o extension or language indicator
     * @return a relative path to the template file.
     */
    protected String getFullTemplateName(String baseName) {
        String fileName = FilenameUtils.concat( getTemplatePath(), baseName);
        if (isWithLanguages()) {
            fileName = fileName.concat(WITH_LANGUAGE);
        }
        return fileName.concat(".ftl");
    }

    protected void initDerivates() {

        this.rootObject.put( LAST_MODIFIED_BY, "system");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        String modificationDate = simpleDateFormat.format(new Date());
        this.rootObject.put( LAST_MODIFICATION_DATE, modificationDate);

        List<String> translationId = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            String randomTranslationId = UUID.randomUUID().toString();
            translationId.add(randomTranslationId);
        }
        this.rootObject.put( TRANSLATION_ID, translationId);
    }

    @Override
    public void render(String baseName) {
        String templateName = getFullTemplateName(baseName);
        super.render(templateName);
    }
}
