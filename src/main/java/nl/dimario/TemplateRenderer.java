package nl.dimario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;

public abstract class TemplateRenderer implements CodegenConstants
{
    private Configuration configuration;
    protected Map<String, Object> rootObject;

    public abstract String getRootDataFileName();


    private void reportError( Exception x) {
        System.out.print( "\n\n\n***   ERROR  ***\n\n");
        System.out.println( x.getMessage());
        x.printStackTrace();
        Throwable t = x.getCause();
        while( t != null) {
            System.out.println( "Caused by");
            System.out.println( t.getMessage());
            t.printStackTrace();
            t = t.getCause();
        }
    }

    public void init( DataSet dataSet) throws IOException {
        this.configuration = new Configuration( Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        this.configuration.setClassForTemplateLoading( TemplateRenderer.class, "/");
        this.rootObject = dataSet.getRootData( getRootDataFileName());
    }

    public void render( String templateName)
    {
        try ( StringWriter writer = new StringWriter()) {

            Template template = configuration.getTemplate( templateName);
            template.process( rootObject, writer);
            String rendered = writer.getBuffer().toString();
            splitOutput( templateName, rendered);

        } catch (Exception x) {
            reportError( x);
        }
    }

    private void splitOutput( String templateName, String rendered) throws IOException {

        SplitFile splitFile  = null;
        try( BufferedReader reader = new BufferedReader( new StringReader( rendered))) {
            String line = reader.readLine();
            while( line != null) {
                if( line.contains( SPLITFILE)) {
                    if (splitFile != null) {
                        splitFile = splitFile.write();
                    }
                    splitFile = new SplitFile(splitFile, line);
                } else if (line.contains(MMMARKER)) {
                    SplitFile.setGlobal( line);
                } else {
                    if( splitFile == null) {
                        splitFile = new SplitFile( null, "");
                        splitFile.initNoMarkers( templateName);
                    }
                    splitFile.append( line);
                }
                line = reader.readLine();
            }
            while( splitFile != null) {
                splitFile = splitFile.write();
            }
        }
    }
}
