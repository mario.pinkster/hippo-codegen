package nl.dimario;

/**
 * One SplitFile buffers content for one output file.
 * The general idea is that one large generated *.yaml content blob
 * is post-processed after generation and split into various files
 * in a directory structure.
 *
 * The directory structure may contain nested directories, so that
 * one single *.ftl template generates one large *.yaml blob which
 * is then distributed across multiple files in multiple levels
 * of subdirectories, with names and rootnodes that are determined
 * during  the rendering of the ftl template.
 *
 * TODO: not sure if this paragraph still reflects actual workings
 * The splitting up of content to various subdirectory levels makes
 * it necessary that we may resume emitting output to a higher level
 * file after finishing with a lower level file. For this reason
 * the SplitFiles are ordered in a nested parent-child hierarchy.
 * Once a child is written, processing of input *.yaml file
 * resumes emitting lines to the parent until that is finished too,
 * upon which emitting lines resumes to the parent's parent.
 *
 * TODO: the "special marker" has been split up into several separate directives
 * The beginning of a new SplitFile is announced with a special marker
 * in the input file. This marker also conveys information about
 * which subdirectory must be created and what the new file is named.
 * The termination of the current SplitFile is also marked with a
 * special marker. The end marker however does not add extra information.
 */

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class SplitFile implements CodegenConstants {

    private String beginLine;
    private boolean initialized;
    String filepath;
    String filename;
    String nodepath;
    private List<String> content;
    private SplitFile parent;
    private boolean isWriteDefConHeader;

    static boolean globalWriteDefConHeader = false;
    static String rootNode = "/";
    static String currentPath = null;
    static String outputPath = "./";
    static String LINE_ENDING = System.lineSeparator();

    /**
     * This constructor receives one line from the rendered output with a marker in it
     * The line is stored and will be processed as soon as we add content.
     *
     * @param beginLine
     */
    public SplitFile( SplitFile parent, String beginLine)  {
        this.parent = parent;
        this.content = new ArrayList<String>();
        this.beginLine = beginLine;
        this.initialized = false;
    }

    /**
     * Initializes a current directory and file name and current node path from a previously received
     * input line that held a special "start of splitfile" marker together with the current values
     * of the global variables that hold the current settings for rootpath and rootnode.
     *
     * @throws IOException in case of syntax errors in the marker line.
     */
    private void init( String line) throws IOException {
        if( this.initialized) {
            return;
        }
        int pos = beginLine.indexOf( SPLITFILE);
        int posEquals = -1;
        if( pos > -1) {
            posEquals = beginLine.indexOf('=', pos);
        }
        if( ! (pos > -1 && posEquals > -1)) {
            throw new IOException( makeErrorMessage(beginLine));
        }

        this.filepath = SplitFile.currentPath;
        String fname = beginLine.substring( posEquals + 1);
        this.filename = FilenameUtils.getName( fname);
        this.nodepath = getNodePath( line);
        this.isWriteDefConHeader = globalWriteDefConHeader;

        this.initialized = true;
    }

    public void initNoMarkers( String fullPathAndName) {
        this.filename = FilenameUtils.getBaseName( fullPathAndName) + ".yaml";
        this.filepath = FilenameUtils.getPath( fullPathAndName);
        this.nodepath = "";
        this.isWriteDefConHeader = globalWriteDefConHeader;
        this.initialized = true;
    }

    /**
     * Appends one (normal) line from the input *.yaml file to the buffer for this SplitFile.
     * For a newly started SplitFile, init() is called to set the directory and filename
     * and the first line of the new file is used to determine the node.
     *
     * The reason we do it here and not in the constructor is that we don't want to throw
     * exceptions from a constructor.
     *
     * @param line the line that must be added to the buffer
     * @throws IOException in case the SplitFile was started due to a merker line with syntax errors
     */
    public void append( String line) throws IOException {
        init( line);
        content.add( line);
    }

    /**
     * Write the contents of our buffer to a file with the name and directory that was set
     * in the special marker line. The path (current working directory) for this particular
     * SplitFile is constructed by concatenating all the directories of our parental hierarchy.
     *
     * After writing ourselves, we return the parent in the hierarchy so that emitting lines
     * to that file may resume. For the highest level in the hierarchy, the parent is null
     * so the controlling program that uses SplitFiles should use this as a sign that there
     * are no SplitFiles left in the hierarchy (the hierarchy has been popped until empty).
     *
     * @return the parent (which is null after the root file of the hierarchy has been written).
     * @throws IOException in case something went wrong while creating or writing the file.
     */

    public SplitFile write() throws IOException {

        File outdir = new File( getFilePath());
        FileUtils.forceMkdir( outdir);
        File outfile = new File( outdir, this.filename);
        String cooked = this.isYaml() ? prepareYamlContent() : prepareScriptContent();
        FileUtils.writeStringToFile( outfile, cooked, StandardCharsets.UTF_8);
        return this.parent;
    }

    /**
     * @return directory path for the current file.
     * This concatenates the global output path (set on the command line) and the
     * current filepath for this splitfile (set with $$$filePath=xxx in the template).
     */
    protected String getFilePath() {
        String result = FilenameUtils.concat( SplitFile.outputPath, this.filepath);
        return result;
    }

    /**
     * @return the full nodepath of this splitfile. It is composed of the node that
     * is read from the first line of the input together with the rootNode value
     * that is currently applicable.
     *
     * @param firstLine = the first line for this new splitfile. It is supposed to
     * be a node definition.
     */

    protected String getNodePath( String firstLine) {
        String rootNode = SplitFile.rootNode;
        if( rootNode.endsWith( ":")) {
            rootNode = StringUtils.chop(rootNode);
        }
        String node = firstLine.trim();
        if( node.endsWith( ":")) {
            node = StringUtils.chop( node);
        }
        if( node.startsWith( "/")) {
            node = node.substring(1);
        }
        String result = FilenameUtils.concat( rootNode, node);
        return result   ;
    }

    /**
     * Convert the list of strings we want to print by first shifting content until it bumps the left margin
     * (while maintaining relative indentation) and then adding definitions: config: header if needed while
     * indenting everything again by 4 extra spaces.
     *
     * At the start of the output, the current complete nodePath is rendered with a correct indentation.
     *
     * Line endings are operating system specific.
     *
     * @return
     */
    String prepareYamlContent() {

        int minLeadingSpace = determineMinLeadingSpace();
        int minIndent = this.isWriteDefConHeader ? 4 : 0;
        int shift = minIndent - minLeadingSpace;
        String indent = "";
        for( int k=0; k<shift; k++) {
            indent = indent + " ";
        }
        StringBuilder sb = new StringBuilder();
        if( this.isWriteDefConHeader) {
            sb.append( "definitions:");
            sb.append(LINE_ENDING);
            sb.append( "  config:");
            sb.append(LINE_ENDING);
            sb.append( "    ");
            sb.append( this.nodepath);
        } else {
            sb.append( this.nodepath);
        }
        sb.append( ":");
        sb.append(LINE_ENDING);
        for( int i=1; i<content.size(); i++) {
            String line = content.get(i);
            if( shift < 0) {
                sb.append( line.substring( -shift));
            } else {
                sb.append( indent);
                sb.append( line);
            }
            sb.append(LINE_ENDING);
        }
        return sb.toString();
    }

    String prepareScriptContent() {
        StringBuilder sb = new StringBuilder();
        for( int i=1; i<content.size(); i++) {
            String line = content.get(i);
            sb.append( line);
            sb.append(LINE_ENDING);
        }
        return sb.toString();
    }

    int determineMinLeadingSpace() {
        int result = 1000;
        for( int i=0; i< content.size(); i++) {
            String line = content.get(i);
            int j = 0;
            while( line.charAt( j) == ' ' && (result > j)) {
                j++;
            }
            if( result > j) {
                result = j;
            }
        }
        return result;
    }

    boolean isYaml() {
        String ext = FilenameUtils.getExtension( this.filename);
        return "yaml".equalsIgnoreCase( ext);
    }

    /**
     * Set one of the global values from the information in the line.
     *
     * @param line
     * @throws IOException
     */

    public static void setGlobal( String line) throws IOException {
        int pos = line.indexOf( MMMARKER);
        if( pos < 0) {
            throw new IOException( makeErrorMessage( line));
        }
        String optionString = line.substring( pos+3);
        String[] pair = optionString.split( "=");
        if( pair.length != 2) {
            throw new IOException( makeErrorMessage( line));
        }
        String option = pair[0];
        String value = pair[1];
        if( value.endsWith( ";")) {
            value = StringUtils.chop( value);
        }
        if( WRITEDEFCONHEADER.equals( option)) {
            SplitFile.globalWriteDefConHeader = Boolean.parseBoolean( value);
        } else if( ROOTNODE.equals(option)) {
            SplitFile.rootNode = value;
        } else if( FILEPATH.equals( option)) {
            SplitFile.currentPath = value;
        } else {
            throw new IOException( makeErrorMessage( line));
        }
    }

    static private String makeErrorMessage( String line) {
        return String.format( "Bad SplitFile syntax: must be \"%soption=value\"  (option = file, path, rootNode, writeDefConHeader)  but was: %s", MMMARKER,
                line);
    }

    /**
     * Set the global output path that is prefixed in front of any other file path.
     *
     * @param outputPath
     */
    static public void setOutputPath( String outputPath) {
        SplitFile.outputPath = outputPath;
    }
}
