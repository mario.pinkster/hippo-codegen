package nl.dimario;

public interface CodegenConstants {

    static final String MMMARKER = "$$$";
    static final String SPLITFILE = MMMARKER  + "file";
    static final String WRITEDEFCONHEADER = "writeDefConHeader";
    static final String ROOTNODE = "rootNode";
    static final String FILEPATH = "path";

    static final String GENERATOR_CONFIG_FILE = "generator.properties";
    static final String HOSTGROUPS = "hostgroups";
    static final String HOSTGROUPS_FILE = "hostgroups.json";
    static final String DATA_DIR = "./data";
    static final String DEFAULT_DATA_DIR = DATA_DIR + "/default";
    static final String OUTPUT_DIR = "output.dir";
    static final String DEFAULT_OUTPUT_DIR = "./output";

}
