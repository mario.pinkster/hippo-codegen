package nl.dimario;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ChannelGenerator extends GeneratorBase {

    private static final String TEMPLATE_PATH = "./channel";

    public static void main(String[] args) {

        DataSet ds = new DataSet();
        try {
            if (ds.init(args)) {
                ChannelGenerator cg = new ChannelGenerator();
                cg.init( ds);
                cg.initDerivates();
                cg.generate( ds);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getRootDataFileName() {
        return CHANNEL_DATA_FILENAME;
    }

    @Override
    public String getTemplatePath() {
        return TEMPLATE_PATH;
    }

    @Override
    protected void initDerivates() {
        super.initDerivates();

        if (this.isWithLanguages()) {
            initDerivatesYesLanguages();
        } else {
            initDerivatesNoLanguages();
        }
    }

    protected void initDerivatesNoLanguages() {

        String displayName = String.format("%S %s %S",
                (String) rootObject.get(BRANCH_NAME),
                (String) rootObject.get(DISPLAY_NAME_PART),
                (String) rootObject.get(COUNTRY_CODE));
        rootObject.put(CHANNEL_DISPLAY_NAME, displayName);

        String hstAlias = String.format("%s-%s",
                (String) rootObject.get(CHANNEL_ID),
                (String) rootObject.get(BRANCH_ID));
        rootObject.put(HST_ALIAS, hstAlias);
    }

    protected void initDerivatesYesLanguages() {

        String branchId = (String) rootObject.get(BRANCH_ID);
        String branchName = (String) rootObject.get(BRANCH_NAME);
        String channelId = (String) rootObject.get(CHANNEL_ID);
        String displayNamePart = (String) rootObject.get(DISPLAY_NAME_PART);

        String hstRootAlias = String.format("%s-%s", channelId, branchId);
        rootObject.put(HST_ROOT_ALIAS, hstRootAlias);

        List<Map> languages = (List<Map>) this.rootObject.get( LANGUAGES);

        for ( Map language : languages) {

            String languageCode = (String) language.get( LANGUAGE_CODE);

            String hstLanguageAlias = String.format("%s-%s-%s",
                    channelId, languageCode, branchId);
            language.put( HST_ALIAS, hstLanguageAlias);

            String channelDisplayName = String.format("%S %s %S",
                    branchName, displayNamePart, languageCode);
            language.put( CHANNEL_DISPLAY_NAME, channelDisplayName);
        }
    }

    protected void generate( DataSet dataSet) throws IOException {

        render(DOCUMENTS_FOLDER_TEMPLATE_NAME);
        render(SYSTEM_DOCUMENTS_FOLDER_TEMPLATE_NAME);
        render(HST_CONFIGURATION_TEMPLATE_NAME);
        render(WORKSPACE_TEMPLATE_NAME);
        generateHostsAndSites( dataSet);
        render(UPDATE_CHANNEL_SELECTOR_SCRIPT_TEMPLATE_NAME);
    }

    protected void generateHostsAndSites( DataSet dataSet) throws IOException {

        String branchId = (String) rootObject.get(BRANCH_ID);
        String channelId = (String) rootObject.get(CHANNEL_ID);
        for( String hostGroup: dataSet.getHostgroups()) {
            String virtualHostName = String.format("%s-%s-%s",
                    hostGroup.replace("-eu", StringUtils.EMPTY), channelId, branchId);
            this.rootObject.put( VIRTUAL_HOST_NAME, virtualHostName);
            this.rootObject.put( HOST_GROUP, hostGroup);
            render( HOSTS_TEMPLATE_NAME);
        }
        render( SITES_TEMPLATE_NAME);
    }
}
