package nl.dimario;

/**
 * Generate yaml configuration (and updater scripts?)
 * specifically for the upgrade from version 12 to XIII
 * There is no language sensitive version.
 */

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

public class CmsHostGenerator extends GeneratorBase {

    protected static final String CMSHOST_XIII_DATA_FILENAME = "cmshost-xiii.json";
    protected static final String CMSHOST_XIII_TEMPLATE_PATH = "./cmshost-xiii";
    protected static final String CMSHOST_TEMPLATE_NAME = "cmshost-xiii";

    public static void main(String[] args) {

        DataSet ds = new DataSet();
        try {
            if (ds.init(args)) {
                CmsHostGenerator chg = new CmsHostGenerator();
                chg.init( ds);
                chg.initDerivates();
                chg.generate( ds);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void generate( DataSet dataSet) throws IOException {

//        String branchId = (String) rootObject.get(BRANCH_ID);
//        String channelId = (String) rootObject.get(CHANNEL_ID);
        for( String hostGroup: dataSet.getHostgroups()) {
            if( hostGroup.contains( "stg-") || hostGroup.contains( "-eu")) {
                continue;
            }
            this.rootObject.put( HOST_GROUP, hostGroup);
//            String virtualHostName = String.format("%s-%s-%s",
//                    hostGroup.replace("-eu", StringUtils.EMPTY), channelId, branchId);
//            this.rootObject.put( VIRTUAL_HOST_NAME, virtualHostName);
//            this.rootObject.put( HOST_GROUP, hostGroup);
            render( CMSHOST_TEMPLATE_NAME);
        }
    }

    @Override
    public String getRootDataFileName() {
        return CMSHOST_XIII_DATA_FILENAME;
    }

    @Override
    protected String getTemplatePath() {
        return CMSHOST_XIII_TEMPLATE_PATH;
    }
}
