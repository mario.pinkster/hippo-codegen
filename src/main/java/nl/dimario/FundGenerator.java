package nl.dimario;

import java.io.IOException;

public class FundGenerator extends GeneratorBase {

    protected static final String FUND_DATA_FILENAME = "fund.json";
    protected static final String FUND_TEMPLATE_PATH = "./fund";
    protected static final String FUND_TEMPLATE_NAME = "fund";



    @Override
    public String getRootDataFileName() {
        return FUND_DATA_FILENAME;
    }


    public static void main(String[] args) {

        DataSet ds = new DataSet();
        try {
            if (ds.init(args)) {
                FundGenerator fg = new FundGenerator();
                fg.init( ds);
                fg.initDerivates();
                fg.generate( ds);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void generate( DataSet dataSet) throws IOException {

    }

    @Override
    protected String getTemplatePath() {
        return FUND_TEMPLATE_PATH;
    }
}
