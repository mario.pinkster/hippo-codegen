<h3>Hippo Code Generator</h3>

**What does it do?**<br>
It generates yaml files and groovy scripts specifically for use with the Hippo CMS.

**How do I use it?**<br>
You put together a set of input json files, place them in a directory under ./data and fire up one of the main() methods with this directory as input arg on the command line. It then creates a directory structure containing *.yaml (and possibly other) files under a root directory with the dataset name under ./output. The structure and naming of the directories and files is such that you can copy it without modification to a Hippo application data directory and thus incorporate it in your project. 

For specific details on what sort of configuration it can create, see the other *.md files in the project root.

**How does it work?**<br>
It uses a set of Freemarker templates plus the input JSON file(s) to generate the output. The templates contain special (non-Freemarker) directives (all starting with **$$$**) that can break up the output of one template across various named output files in different directories (which are created as needed) while at the same time defining the root node for each file and adjusting indentation as needed. The glue between the JSON input data and the templates is formed by the  various Java *Generator classes.

**How can I adapt it to my own purpose?**<br>
You will need to do these things, broadly speaking:

* Figure out what variable input data you will need, how to structure it in JSON, and how this data is used in the Freemarker template(s).
* Write a Java generator class specifically for the type of output you need. This class prepares the input data to be used in the Freemarker  templates and renders one or more templates as needed.
* Write one or more Freemarker templates to be driven by the Java generator.


For examples of these see the existing Java *Generator classes, the templates in the resource folder, and the JSON files in ./data.