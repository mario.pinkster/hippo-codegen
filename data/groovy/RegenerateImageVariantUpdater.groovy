package com.triodos.cms.admin.updater

import com.triodos.cms.gallery.editor.crop.ImageVariantService
import org.hippoecm.frontend.plugins.gallery.imageutil.ImageUtils
import org.hippoecm.frontend.plugins.gallery.imageutil.ScalingParameters
import org.hippoecm.frontend.plugins.gallery.model.GalleryException
import org.hippoecm.frontend.plugins.gallery.processor.ScalingGalleryProcessor
import org.hippoecm.repository.util.JcrUtils
import org.onehippo.repository.update.BaseNodeUpdateVisitor

import javax.jcr.Node
import javax.jcr.RepositoryException
import javax.jcr.Session

class RegenerateImageVariant extends BaseNodeUpdateVisitor {

    private static final int DEFAULT_WIDTH = 0;
    private static final int DEFAULT_HEIGHT = 0;
    private static final boolean DEFAULT_UPSCALING = false;
    private static final String DEFAULT_OPTIMIZE = "quality";
    private static final double DEFAULT_COMPRESSION = 1.0;

    private static final String CONFIG_PARAM_WIDTH = "width";
    private static final String CONFIG_PARAM_HEIGHT = "height";
    private static final String CONFIG_PARAM_UPSCALING = "upscaling";
    private static final String CONFIG_PARAM_OPTIMIZE = "optimize";
    private static final String CONFIG_PARAM_COMPRESSION = "compression";


    private static final Map<String, ImageUtils.ScalingStrategy> SCALING_STRATEGY_MAP = new LinkedHashMap<>();
    static {
        SCALING_STRATEGY_MAP.put("auto", ImageUtils.ScalingStrategy.AUTO);
        SCALING_STRATEGY_MAP.put("speed", ImageUtils.ScalingStrategy.SPEED);
        SCALING_STRATEGY_MAP.put("speed.and.quality", ImageUtils.ScalingStrategy.SPEED_AND_QUALITY);
        SCALING_STRATEGY_MAP.put("quality", ImageUtils.ScalingStrategy.QUALITY);
        SCALING_STRATEGY_MAP.put("best.quality", ImageUtils.ScalingStrategy.BEST_QUALITY);
    }

    private ScalingGalleryProcessor galleryProcessor;

    @Override
    void initialize(Session session) throws RepositoryException {
        super.initialize(session);

        galleryProcessor = new ScalingGalleryProcessor();

        def galleryProcessorServiceNode = session.getNode("/hippo:configuration/hippo:frontend/cms/cms-services/galleryProcessorService");
        def nodeIterator = galleryProcessorServiceNode.getNodes();
        while (nodeIterator.hasNext()) {
            def node = nodeIterator.nextNode();
            def nodeName = node.getName();

            def width = JcrUtils.getLongProperty(node, CONFIG_PARAM_WIDTH, DEFAULT_WIDTH) as Integer;
            def height = JcrUtils.getLongProperty(node, CONFIG_PARAM_HEIGHT, DEFAULT_HEIGHT) as Integer;
            def upscaling = JcrUtils.getBooleanProperty(node, CONFIG_PARAM_UPSCALING, DEFAULT_UPSCALING);
            def compressionQuality = JcrUtils.getDoubleProperty(node, CONFIG_PARAM_COMPRESSION, DEFAULT_COMPRESSION) as Float;

            def strategyName = JcrUtils.getStringProperty(node, CONFIG_PARAM_OPTIMIZE, DEFAULT_OPTIMIZE);
            ImageUtils.ScalingStrategy strategy = SCALING_STRATEGY_MAP.get(strategyName);
            if (strategy == null) {
                strategy = SCALING_STRATEGY_MAP.get(DEFAULT_OPTIMIZE);
            }

            ScalingParameters parameters = new ScalingParameters(width, height, upscaling, strategy, compressionQuality);
            log.debug("Scaling parameters for {}: {}", nodeName, parameters);
            galleryProcessor.addScalingParameters(nodeName, parameters);
        }
    }

    @Override
    boolean doUpdate(Node node) throws RepositoryException {
        log.debug "Regenerating image variant: ${node.path}"
        regenerateImageVariant(node);

        return true
    }

    @Override
    boolean undoUpdate(Node node) throws RepositoryException, UnsupportedOperationException {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method');
    }

    private void regenerateImageVariant(final Node imageVariantNode) {
        try {
            def imageVariantService = new ImageVariantService(galleryProcessor);

            if (imageVariantService.isCroppedImage(imageVariantNode)) {
                def width = (int) imageVariantNode.getProperty(ImageVariantService.CROPPING_WINDOW_WIDTH).getValue().long;
                def height = (int) imageVariantNode.getProperty(ImageVariantService.CROPPING_WINDOW_HEIGHT).getValue().long;
                def top = (int) imageVariantNode.getProperty(ImageVariantService.CROPPING_WINDOW_TOP_OFFSET).getValue().long;
                def left = (int) imageVariantNode.getProperty(ImageVariantService.CROPPING_WINDOW_LEFT_OFFSET).getValue().long;

                imageVariantService.cropImageVariant(imageVariantNode, top, height, left, width);
            } else {
                imageVariantService.generateImageVariant(imageVariantNode);
            }
        } catch (GalleryException | IOException | RepositoryException ex) {
            log.error("Unable to regenerate image variant", ex);
        }
    }
}