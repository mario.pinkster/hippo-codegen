package com.triodos.cms.admin.updater

import com.triodos.common.TriodosNodeTypes
import org.hippoecm.repository.util.JcrUtils

import javax.jcr.*

class UpdateTriodosHippoDocumentTypeUpdater extends BaseNodeUpdater {

    private static final String DOCUMENT_TYPE_PROTOTYPE_PATH = "/hippo:namespaces/triodoshippo/%s/hipposysedit:prototypes/hipposysedit:prototype"

    private Map<String, Property> documentTypeProperties = new HashMap()
    private Map<String, Node> documentTypeChildNodes = new HashMap()

    @Override
    void initialize(Session session) throws RepositoryException {
        String documentTypePrototypePath = String.format(DOCUMENT_TYPE_PROTOTYPE_PATH, parametersMap.get("documentType"))
        Node documentTypePrototype = session.getNode(documentTypePrototypePath)

        PropertyIterator propertyIterator = documentTypePrototype.getProperties(TriodosNodeTypes.TRIODOS_HIPPO_NAMESPACE + "*")
        while (propertyIterator.hasNext()) {
            Property property = propertyIterator.nextProperty()
            documentTypeProperties.put(property.getName(), property)
        }

        log.debug("Namespace prototype has '{}' '{}' properties", documentTypeProperties.size(), TriodosNodeTypes.TRIODOS_HIPPO_NAMESPACE)

        Iterator childNodeIterator = documentTypePrototype.getNodes()
        while (childNodeIterator.hasNext()) {
            Node childNode = childNodeIterator.nextNode()
            documentTypeChildNodes.put(childNode.getName(), childNode)
        }

        log.debug("Namespace prototype has '{}' child nodes", documentTypeChildNodes.size())
    }

    @Override
    boolean doUpdate(Node node) throws RepositoryException {
        if (!node.isNodeType(TriodosNodeTypes.TRIODOS_HIPPO_NAMESPACE + parametersMap.get("documentType"))) {
            return false
        }

        if (parametersMap.get("removeProperties"))
            removeSuperfluousProperties(node)

        if (parametersMap.get("addProperties"))
            addMissingProperties(node)

        if (parametersMap.get("removeChildNodes"))
            removeSuperfluousChildNodes(node)

        if (parametersMap.get("addChildNodes"))
            addMissingChildNodes(node)

        return true
    }

    private void addMissingProperties(Node node) {
        for (Map.Entry<String, Property> entry : documentTypeProperties.entrySet()) {
            String documentTypePropertyName = entry.getKey()
            Property documentTypeProperty = entry.getValue()
            if (!node.hasProperty(documentTypePropertyName)) {
                log.debug("Node '{}' does not have property '{}' that is on the prototype", node.getPath(), documentTypePropertyName)
                if (!documentTypeProperty.isMultiple()) {
                    node.setProperty(documentTypePropertyName, documentTypeProperty.getValue(), documentTypeProperty.getType())
                    log.debug("Single value property '{}' has been set", documentTypePropertyName)
                } else {
                    node.setProperty(documentTypePropertyName, documentTypeProperty.getValues(), documentTypeProperty.getType())
                    log.debug("Multi-valued property '{}' has been set", documentTypePropertyName)
                }
            }
        }
    }

    private void removeSuperfluousProperties(Node node) {
        PropertyIterator documentPropertyIterator = node.getProperties(TriodosNodeTypes.TRIODOS_HIPPO_NAMESPACE + "*")
        while (documentPropertyIterator.hasNext()) {
            Property documentProperty = documentPropertyIterator.nextProperty()
            if (!documentTypeProperties.keySet().contains(documentProperty.getName())) {
                log.debug("Property '{}' on node '{}' is not in the prototype", documentProperty.getName(), node.getPath())
                node.getProperty(documentProperty.getName()).remove()
                log.debug("Removed property '{}'", documentProperty.getName())
            }
        }
    }

    private void addMissingChildNodes(Node node) {
        for (Map.Entry<String, Node> entry : documentTypeChildNodes) {
            String documentTypeChildNodeName = entry.getKey()
            Node documentTypeChildNode = entry.getValue()
            if (!node.hasNode(documentTypeChildNodeName)) {
                log.debug("Node '{}' does not have child node '{}' that is on the prototype", node.getPath(), documentTypeChildNodeName)
                JcrUtils.copy(node.getSession(), documentTypeChildNode.getPath(), node.getPath() + "/" + documentTypeChildNode.getName())
                log.debug("Node '{}' has been added", documentTypeChildNodeName)
            }
        }
    }

    private void removeSuperfluousChildNodes(Node node) {
        NodeIterator documentChildNodeIterator = node.getNodes()
        while (documentChildNodeIterator.hasNext()) {
            Node documentChildNode = documentChildNodeIterator.nextNode()
            if (!documentTypeChildNodes.keySet().contains(documentChildNode.getName())) {
                log.debug("Child node '{}' on node '{}' is not in the prototype", documentChildNode.getName(), node.getPath())
                node.getNode(documentChildNode.getName()).remove()
                log.debug("Removed child node '{}'", documentChildNode.getName())
            }
        }
    }

    @Override
    boolean undoUpdate(Node node) throws RepositoryException, UnsupportedOperationException {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
