package com.triodos.cms.admin.updater

import org.hippoecm.hst.configuration.HstNodeTypes

import javax.jcr.Node

/**
 * This updater adds fund generic prototype pages to the workspace of the channel that is in the input data.
 * It uses the same input data as the updater that adds sitemap items for the funds pages.
 */
class AddFundPageGenericPageUpdater extends BaseNodeUpdater {

    boolean doUpdate(Node configurationsNode) {
        List<Map<String, Object>> channels = (List<Map<String, Object>>) parametersMap.get("channels")
        for (Map<String, Object> channel : channels) {
            addFundPageGenericPages(configurationsNode, (String) channel.get("name"), channel)
            addFundPageGenericPages(configurationsNode, String.format("%s-preview", channel.get("name")), channel)
        }

        return true
    }

    void addFundPageGenericPages(Node configurationsNode, String channelName, Map<String, Object> channel) {
        if (configurationsNode.hasNode(channelName)) {
            Node pagesNode = configurationsNode.getNode(channelName).getNode("hst:workspace/hst:pages");
            if (pagesNode != null) {
                for (String fundName : (List<String>) channel.get("funds")) {
                    String pageName = String.format("%s-%s-generic-page-prototype", channel.get("sitemap"), fundName)
                    addFundPageGenericPagePrototype(pagesNode, pageName)
                }
            } else {
                log.warn "Failed updating fund pages for channel '${channelName}', as there is no root pages node!"
            }
        } else {
            log.debug "Skipping updating fund pages for channel '${channelName}', as this channel does not exist!"
        }
    }

    void addFundPageGenericPagePrototype(Node pagesNode, String pageName) {
        if (!pagesNode.hasNode(pageName)) {
            Node pageNode = pagesNode.addNode(pageName, HstNodeTypes.NODETYPE_HST_COMPONENT)
            pageNode.setProperty(HstNodeTypes.COMPONENT_PROPERTY_REFERECENCECOMPONENT, "hst:abstractpages/base")

            Node mainNode = pageNode.addNode("main", HstNodeTypes.NODETYPE_HST_COMPONENT)
            Node containerNode = mainNode.addNode("container", HstNodeTypes.NODETYPE_HST_CONTAINERCOMPONENT)

            containerNode.setProperty(HstNodeTypes.COMPONENT_PROPERTY_XTYPE, "hst.nomarkup")
            log.debug "Added fund generic prototype page '${pageNode.path}'"
        } else {
            log.debug "Skipped adding fund generic page prototype '${pageName}' for '${pagesNode.path}', as this fund generic page prototype already exists!"
        }
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
