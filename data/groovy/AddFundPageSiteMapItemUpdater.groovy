package com.triodos.cms.admin.updater

import org.hippoecm.hst.configuration.HstNodeTypes

import javax.jcr.Node

/**
 * This updater adds fund sitemap items to the correct channel & root funds sitemap item.
 * It was created initially after we found out that newly added sitemap items & pages for new fund pages are not "initialized" correctly,
 * as the root funds sitemap item (e.g. /funds) was bootstrapped as content. And as we can't reload content, we created this updater instead.
 *
 * NOTE: in july 2019 a separate updater script was added for creating the generic prototype pages in the workspace.
 * It uses the same input data as this script.
 */
class AddFundPageSiteMapItemUpdater extends BaseNodeUpdater {

    boolean doUpdate(Node configurationsNode) {
        List<Map<String, Object>> channels = (List<Map<String, Object>>) parametersMap.get("channels")
        for (Map<String, Object> channel : channels) {
            addFundPageSiteMapItems(configurationsNode, (String) channel.get("name"), channel)
            addFundPageSiteMapItems(configurationsNode, String.format("%s-preview", channel.get("name")), channel)
        }

        return true
    }

    void addFundPageSiteMapItems(Node configurationsNode, String channelName, Map<String, Object> channel) {
        if (configurationsNode.hasNode(channelName)) {
            Node fundsNode = configurationsNode.getNode(channelName).getNode("hst:workspace/hst:sitemap").getNode((String) channel.get("sitemap"))
            if (fundsNode != null) {
                for (String fundName : (List<String>) channel.get("funds")) {
                    addFundPageSiteMapItem(fundsNode, fundName, (String) channel.get("content"), (boolean) channel.get("disclosure"))
                }
            } else {
                log.warn "Failed updating fund pages for channel '${channelName}', as there is no root funds sitemap item!"
            }
        } else {
            log.debug "Skipping updating fund pages for channel '${channelName}', as this channel does not exist!"
        }
    }

    void addFundPageSiteMapItem(Node fundsNode, String fundName, String contentRoot, boolean disclosure) {
        if (!fundsNode.hasNode(fundName)) {
            String componentConfigurationId = String.format("hst:pages/%s-%s-generic-page-prototype", fundsNode.name, fundName)
            String relativeContentPath = String.format("%s/funds/%s/%s", contentRoot, fundName, fundName)

            Node fundNode = fundsNode.addNode(fundName, HstNodeTypes.NODETYPE_HST_SITEMAPITEM)
            fundNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_COMPONENTCONFIGURATIONID, componentConfigurationId)
            fundNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_RELATIVECONTENTPATH, relativeContentPath)
            fundNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_SITEMAPITEMHANDLERIDS, disclosure
                ? ["fund-exists-site-map-item-handler", "disclosure-site-map-item-handler", "fund-priority-site-map-item-handler"] as String[]
                : ["fund-exists-site-map-item-handler", "fund-priority-site-map-item-handler"] as String[])

            Node wildCardNode = fundNode.addNode("_default_", HstNodeTypes.NODETYPE_HST_SITEMAPITEM)
            wildCardNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_COMPONENTCONFIGURATIONID, componentConfigurationId)
            wildCardNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_EXCLUDEDFORLINKREWRITING, true)
            wildCardNode.setProperty(HstNodeTypes.GENERAL_PROPERTY_PARAMETER_NAMES, ["isin"] as String[])
            wildCardNode.setProperty(HstNodeTypes.GENERAL_PROPERTY_PARAMETER_VALUES, ["\${1}"] as String[])
            wildCardNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_RELATIVECONTENTPATH, relativeContentPath)
            wildCardNode.setProperty(HstNodeTypes.SITEMAPITEM_PROPERTY_SITEMAPITEMHANDLERIDS, disclosure
                ? ["fund-exists-site-map-item-handler", "disclosure-site-map-item-handler", "fund-allowed-site-map-item-handler"] as String[]
                : ["fund-exists-site-map-item-handler", "fund-allowed-site-map-item-handler"] as String[])
            log.debug "Added fund page sitemap item '${fundNode.path}'"
        } else {
            log.debug "Skipped adding fund page sitemap item '${fundName}' for '${fundsNode.path}', as this fund page sitemap item already exists!"
        }
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
