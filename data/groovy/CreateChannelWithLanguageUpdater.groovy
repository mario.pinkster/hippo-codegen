package com.triodos.cms.admin.updater

import com.triodos.cms.admin.updater.generator.ChannelGeneratorWithLanguage

import javax.jcr.Node
import javax.jcr.RepositoryException

class CreateChannelWithLanguageUpdater extends ChannelGeneratorWithLanguage {

    @Override
    boolean doUpdate(Node jcrRootNode) throws RepositoryException {
        return super.doUpdate(jcrRootNode)
    }

    @Override
    boolean undoUpdate(Node node) throws RepositoryException, UnsupportedOperationException {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
