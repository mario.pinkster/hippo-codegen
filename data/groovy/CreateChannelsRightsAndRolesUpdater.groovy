package com.triodos.cms.admin.updater

import com.triodos.cms.services.FreemarkerService
import com.triodos.cms.services.hippo.HippoYamlImportService
import org.springframework.beans.factory.annotation.Autowired

import javax.jcr.Node

class CreateChannelsRightsAndRolesUpdater extends BaseNodeUpdater {

    private static final String TEMPLATE_ROOT_DOMAINS = "templates/yaml/rightsandroles/domains"

    @Autowired protected FreemarkerService freemarkerService;
    @Autowired protected HippoYamlImportService hippoYamlImportService;

    boolean doUpdate(Node jcrRootNode) {
        List<Map<String, String>> channels = (List<Map<String, String>>) parametersMap.get("channels")

        Node domainRootNode = jcrRootNode.getNode("hippo:configuration/hippo:domains")

        for (Map<String, String> channel : channels) {
            log.debug("Adding groups and roles for {}", channel.get("channelDisplayName"))

            createRightsAndRolesForChannel(domainRootNode, channel)
        }
        return true
    }
    private void createRightsAndRolesForChannel(Node domainRootNode, Map<String, String> channelModel) {
        log.debug("Creating {} channel content domain", channelModel.get("hstAlias"))
        String branchDomainBlogContent = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS +"/triodos-branch-channel-content.ftl", channelModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainBlogContent)

        log.debug("creating {} channel hstconfig domain", channelModel.get("hstAlias"))
        Node hstconfigNode = domainRootNode.getNode("triodos-" +channelModel.get("branchName") +"-hstconfig");
        String branchDomainBlogHstConfig = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS +"/triodos-channel-hstconfig.ftl", channelModel)
        hippoYamlImportService.importYamlAsString(hstconfigNode, branchDomainBlogHstConfig)

    }
    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
