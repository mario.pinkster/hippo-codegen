package com.triodos.cms.admin.updater

import org.apache.commons.lang.StringUtils
import org.hippoecm.repository.util.JcrUtils

import javax.jcr.Node

class ResourceBundleEntryUpdater extends ResourceBundleUpdater {

    boolean doUpdate(Node node) {
        String currentResourceBundleId = JcrUtils.getStringProperty(node, RESOURCEBUNDLE_ID, StringUtils.EMPTY);

        Map<String, Object> resourceBundles = parametersMap.get(currentResourceBundleId);
        if (resourceBundles != null) {
            for (Map.Entry<String, Object> resourceBundle : resourceBundles.entrySet()) {
                String key = resourceBundle.getKey();
                Object value = resourceBundle.getValue();

                if (StringUtils.isNotBlank(key)) {
                    if (!"DELETE".equals(value)) {
                        addOrUpdateResourceBundleEntry(node, key, value);
                    } else {
                        removeResourceBundleEntry(node, key);
                    }
                }
            }
        }

        return true;
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method');
    }
}
