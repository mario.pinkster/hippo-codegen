package com.triodos.cms.admin.updater

import com.triodos.cms.services.FreemarkerService
import com.triodos.cms.services.hippo.HippoYamlImportService
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired

import javax.jcr.Node

class CreateRightsAndRolesUpdater extends BaseNodeUpdater {

    private static final String TEMPLATE_ROOT_DOMAINS = "templates/yaml/rightsandroles/domains"
    private static final String TEMPLATE_ROOT_GROUPS = "templates/yaml/rightsandroles/groups"

    @Autowired
    protected FreemarkerService freemarkerService;
    @Autowired
    protected HippoYamlImportService hippoYamlImportService;

    boolean doUpdate(Node jcrRootNode) {
        List<Map<String, String>> branches = (List<Map<String, String>>) parametersMap.get("branches")

        Node domainRootNode = jcrRootNode.getNode("hippo:configuration/hippo:domains")
        Node groupRootNode = jcrRootNode.getNode("hippo:configuration/hippo:groups")

        for (Map<String, String> branch : branches) {
            log.debug("Adding groups and roles for branch {}", branch.get("displayName"))

            createGroupForBranch(groupRootNode, branch)
            createRightsAndRolesForBranch(domainRootNode, branch)
        }
        return true
    }

    private void createGroupForBranch(Node groupRootNode, Map<String, String> branchModel) {
        log.debug("Creating {} bulk author group", branchModel.get("displayName"))
        String branchBulkAuthorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-bulk-author.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchBulkAuthorYaml)

        log.debug("Creating {} bulk editor group", branchModel.get("displayName"))
        String branchBulkEditorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-bulk-editor.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchBulkEditorYaml)

        log.debug("Creating {} strip author group", branchModel.get("displayName"))
        String branchStripAuthorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-strip-author.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchStripAuthorYaml)

        log.debug("Creating {} strip editor group", branchModel.get("displayName"))
        String branchStripEditorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-strip-editor.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchStripEditorYaml)

        log.debug("Creating {} channelmanager editor group", branchModel.get("displayName"))
        String branchChannelManagerEditorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-channelmanager-editor.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchChannelManagerEditorYaml)

        log.debug("Creating {} channelsettings editor group", branchModel.get("displayName"))
        String branchChannelSettingsEditorYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-channelsettings-editor.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchChannelSettingsEditorYaml)

        log.debug("Creating {} kvm approve group", branchModel.get("displayName"))
        String branchKvmApproveYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-kvm-approve.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchKvmApproveYaml)

        log.debug("Creating {} kvm change group", branchModel.get("displayName"))
        String branchKvmChangeYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-kvm-change.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchKvmChangeYaml)

        log.debug("Creating {} kvm create group", branchModel.get("displayName"))
        String branchKvmCreateYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-kvm-create.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchKvmCreateYaml)

        log.debug("Creating {} kvm takeoffline group", branchModel.get("displayName"))
        String branchKvmTakeOfflineYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-kvm-takeoffline.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchKvmTakeOfflineYaml)

        log.debug("Creating {} urlrewrite editor group", branchModel.get("displayName"))
        String branchUrlRewriteYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_GROUPS + "/branch-urlrewrite-editor.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(groupRootNode, branchUrlRewriteYaml)
    }

    private void createRightsAndRolesForBranch(Node domainRootNode, Map<String, String> branchModel) {
        log.debug("Creating {} corporate content domain", branchModel.get("displayName"))
        String branchDomainCorporateYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-corporate-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainCorporateYaml)

        log.debug("Creating {} shared content domain", branchModel.get("displayName"))
        String branchDomainSharedYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-shared-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainSharedYaml)

        log.debug("Creating {} shared other content domain", branchModel.get("displayName"))
        String branchDomainSharedOtherYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-shared-other-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainSharedOtherYaml)

        log.debug("Creating {} system content domain", branchModel.get("displayName"))
        String branchDomainSystemYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-system-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainSystemYaml)

        log.debug("Creating {} url rewrite content domain", branchModel.get("displayName"))
        String branchDomainUrlRewriteYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-urlrewrite-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainUrlRewriteYaml)

        log.debug("Creating {} key value manager content domain", branchModel.get("displayName"))
        String branchDomainKeyValueManagerYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-key-value-manager-content.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainKeyValueManagerYaml)

        log.debug("Creating {} frontend config shortcuts exclude domain - article", branchModel.get("displayName"))
        branchModel.put("documentType", "article");
        String branchDomainFrontendConfigShortcutsExcludeYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-frontendconfig-shortcuts-exclude.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode.getNode("frontendconfig/frontend-plugins"), branchDomainFrontendConfigShortcutsExcludeYaml)
        branchModel.remove("documentType");

        log.debug("Creating {} frontend config shortcuts exclude domain - press release", branchModel.get("displayName"))
        branchModel.put("documentType", "press-release");
        branchDomainFrontendConfigShortcutsExcludeYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-frontendconfig-shortcuts-exclude.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode.getNode("frontendconfig/frontend-plugins"), branchDomainFrontendConfigShortcutsExcludeYaml)
        branchModel.remove("documentType");

        log.debug("Creating {} frontend config shortcuts include domain", branchModel.get("displayName"))
        String branchDomainFrontendConfigShortcutsIncludeYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-frontendconfig-shortcuts-include.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainFrontendConfigShortcutsIncludeYaml)

        log.debug("Creating {} hstconfig domain", branchModel.get("displayName"))
        String branchDomainHstConfigYaml = freemarkerService.parseTemplateWithModel(TEMPLATE_ROOT_DOMAINS + "/triodos-branch-hstconfig.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(domainRootNode, branchDomainHstConfigYaml)

        /*
        *
        * Adding section
        *
        * Due to limitations of YAML import 'operations' are not supported therefore we code it
        *
        */
        String branchName = branchModel.get("branchName")

        //Defaultwrite
        log.debug("Adding {} groups to defaultwrite", branchModel.get("displayName"))
        List<String> defaultWriteNodeReadWrite = Arrays.asList(
            branchName + "-kvm-create", branchName + "-kvm-change", branchName + "-kvm-approve", branchName + "-kvm-takeoffline",
            branchName + "-bulk-author", branchName + "-strip-author", branchName + "-bulk-editor", branchName + "-strip-editor",
            branchName + "-channelmanager-editor", branchName + "-channelsettings-editor", branchName + "-urlrewrite-editor")

        Node defaultWriteNode = domainRootNode.getNode("defaultwrite")
        nodePropertyService.addValuesToMultiValuedProperty(defaultWriteNode.getNode("readwrite"), "hipposys:groups", defaultWriteNodeReadWrite)

        //Assets
        log.debug("Adding {} groups to assets domain", branchModel.get("displayName"))
        List<String> assetNodeReadWrite = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author", branchName + "-strip-author")
        List<String> assetNodeAuthor = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author", branchName + "-strip-author")
        List<String> assetNodeEditor = Arrays.asList(branchName + "-kvm-change",
            branchName + "-kvm-approve", branchName + "-kvm-takeoffline", branchName + "-bulk-editor", branchName + "-strip-editor",
            branchName + "-channelmanager-editor", branchName + "-channelsettings-editor", branchName + "-urlrewrite-editor")

        Node assetsNode = domainRootNode.getNode("triodos-assets")
        nodePropertyService.addValuesToMultiValuedProperty(assetsNode.getNode("readwrite"), "hipposys:groups", assetNodeReadWrite)
        nodePropertyService.addValuesToMultiValuedProperty(assetsNode.getNode("author"), "hipposys:groups", assetNodeAuthor)
        nodePropertyService.addValuesToMultiValuedProperty(assetsNode.getNode("editor"), "hipposys:groups", assetNodeEditor)

        //Documents
        log.debug("Adding {} groups to documents domain", branchModel.get("displayName"))
        List<String> documentNodeReadOnly = Arrays.asList(branchName + "-kvm-create", branchName + "-kvm-change", branchName + "-kvm-approve",
            branchName + "-kvm-takeoffline", branchName + "-bulk-author", branchName + "-bulk-editor", branchName + "-channelmanager-editor",
            branchName + "-channelsettings-editor", branchName + "-strip-author", branchName + "-strip-editor", branchName + "-urlrewrite-editor")

        Node documentsNode = domainRootNode.getNode("triodos-documents")
        nodePropertyService.addValuesToMultiValuedProperty(documentsNode.getNode("readonly"), "hipposys:groups", documentNodeReadOnly)

        //Gallery
        log.debug("Adding {} groups to gallery domain", branchModel.get("displayName"))
        List<String> galleryNodeReadWrite = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author", branchName + "-strip-author")
        List<String> galleryNodeAuthor = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author", branchName + "-strip-author")
        List<String> galleryNodeEditor = Arrays.asList(branchName + "-kvm-change",
            branchName + "-kvm-approve", branchName + "-kvm-takeoffline", branchName + "-bulk-editor", branchName + "-strip-editor",
            branchName + "-channelmanager-editor", branchName + "-channelsettings-editor", branchName + "-urlrewrite-editor")

        Node galleryNode = domainRootNode.getNode("triodos-gallery")
        nodePropertyService.addValuesToMultiValuedProperty(galleryNode.getNode("readwrite"), "hipposys:groups", galleryNodeReadWrite)
        nodePropertyService.addValuesToMultiValuedProperty(galleryNode.getNode("author"), "hipposys:groups", galleryNodeAuthor)
        nodePropertyService.addValuesToMultiValuedProperty(galleryNode.getNode("editor"), "hipposys:groups", galleryNodeEditor)

        //Frontendconfig-key-value-manager-perspective
        log.debug("Adding {} groups to frontendconfig key value manager perspective domain", branchModel.get("displayName"))
        List<String> frontendConfigKeyValueManagerPerspectiveNodeReadOnly = Arrays.asList(branchName + "-kvm-change", branchName + "-kvm-approve")

        Node frontendConfigKeyValueManagerPerspectiveNode = domainRootNode.getNode("triodos-frontendconfig-key-value-manager-perspective-include")
        nodePropertyService.addValuesToMultiValuedProperty(frontendConfigKeyValueManagerPerspectiveNode.getNode("readonly"), "hipposys:groups", frontendConfigKeyValueManagerPerspectiveNodeReadOnly)

        //Urlrewrite-section
        log.debug("Adding {} groups to urlrewrite section domain", branchModel.get("displayName"))
        List<String> frontendConfigUrlrewriteSectionNodeReadOnly = Arrays.asList(branchName + "-urlrewrite-editor")

        Node frontendConfigUrlrewriteSectionNode = domainRootNode.getNode("triodos-frontendconfig-urlrewrite-section-include")
        nodePropertyService.addValuesToMultiValuedProperty(frontendConfigUrlrewriteSectionNode.getNode("readonly"), "hipposys:groups", frontendConfigUrlrewriteSectionNodeReadOnly)

        //Login
        log.debug("Adding {} groups to login domain", branchModel.get("displayName"))
        List<String> loginEditor = Arrays.asList(branchName + "-kvm-change", branchName + "-kvm-approve", branchName + "-kvm-takeoffline",
            branchName + "-bulk-editor", branchName + "-strip-editor", branchName + "-channelmanager-editor",
            branchName + "-channelsettings-editor", branchName + "-urlrewrite-editor")
        List<String> loginAuthor = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author", branchName + "-strip-author")

        Node loginNode = domainRootNode.getNode("triodos-login")
        nodePropertyService.addValuesToMultiValuedProperty(loginNode.getNode("author"), "hipposys:groups", loginAuthor)
        nodePropertyService.addValuesToMultiValuedProperty(loginNode.getNode("editor"), "hipposys:groups", loginEditor)

        //Targeting
        log.debug("Adding {} groups to targeting domain", branchModel.get("displayName"))
        List<String> targetingReadWrite = Arrays.asList(branchName + "-channelmanager-editor", branchName + "-channelsettings-editor")

        Node targetingNode = domainRootNode.getNode("targeting")
        nodePropertyService.addValuesToMultiValuedProperty(targetingNode.getNode("readwrite"), "hipposys:groups", targetingReadWrite)

        //Targeting Security
        log.debug("Adding {} groups to targeting security domain", branchModel.get("displayName"))
        List<String> targetingSecurityReadOnly = Arrays.asList(branchName + "-channelmanager-editor", branchName + "-channelsettings-editor")

        Node targetingSecurityNode = domainRootNode.getNode("targeting-security")
        nodePropertyService.addValuesToMultiValuedProperty(targetingSecurityNode.getNode("readonly"), "hipposys:groups", targetingSecurityReadOnly)

        //Webfiles
        log.debug("Adding {} groups to webfiles domain", branchModel.get("displayName"))
        List<String> webfilesReadOnly = Arrays.asList(branchName + "-channelmanager-editor", branchName + "-channelsettings-editor")

        Node webfilesNode = domainRootNode.getNode("webfiles")
        nodePropertyService.addValuesToMultiValuedProperty(webfilesNode.getNode("readonly"), "hipposys:groups", webfilesReadOnly)

        //Binaries
        log.debug("Adding {} groups to binaries domain", branchModel.get("displayName"))
        List<String> binariesReadOnly = Arrays.asList(branchName + "-bulk-author", branchName + "-bulk-editor", branchName + "-channelmanager-editor",
            branchName + "-channelsettings-editor", branchName + "-kvm-create", branchName + "-kvm-change", branchName + "-kvm-approve",
            branchName + "-kvm-takeoffline", branchName + "-strip-author", branchName + "-strip-editor", branchName + "-urlrewrite-editor")

        Node binariesNode = domainRootNode.getNode("triodos-templatequery-binaries")
        nodePropertyService.addValuesToMultiValuedProperty(binariesNode.getNode("readonly"), "hipposys:groups", binariesReadOnly)

        //Bulk
        log.debug("Adding {} groups to bulk domain", branchModel.get("displayName"))
        List<String> bulkReadOnly = Arrays.asList(branchName + "-bulk-author", branchName + "-bulk-editor")

        Node bulkNode = domainRootNode.getNode("triodos-templatequery-bulk")
        nodePropertyService.addValuesToMultiValuedProperty(bulkNode.getNode("readonly"), "hipposys:groups", bulkReadOnly)

        //Channelsettings
        log.debug("Adding {} groups to channelsettings domain", branchModel.get("displayName"))
        List<String> channelSettingsReadOnly = Arrays.asList(branchName + "-channelsettings-editor")

        Node channelSettingsNode = domainRootNode.getNode("triodos-templatequery-channelsettings")
        nodePropertyService.addValuesToMultiValuedProperty(channelSettingsNode.getNode("readonly"), "hipposys:groups", channelSettingsReadOnly)

        //KeyValueManager
        log.debug("Adding {} groups to key value manager domain", branchModel.get("displayName"))
        List<String> keyValueManagerReadOnly = Arrays.asList(branchName + "-kvm-create")

        Node keyValueManagerNode = domainRootNode.getNode("triodos-templatequery-key-value-manager")
        nodePropertyService.addValuesToMultiValuedProperty(keyValueManagerNode.getNode("readonly"), "hipposys:groups", keyValueManagerReadOnly)

        //Strip
        log.debug("Adding {} groups to strip domain", branchModel.get("displayName"))
        List<String> stripReadOnly = Arrays.asList(branchName + "-strip-author", branchName + "-strip-editor")

        Node stripNode = domainRootNode.getNode("triodos-templatequery-strip")
        nodePropertyService.addValuesToMultiValuedProperty(stripNode.getNode("readonly"), "hipposys:groups", stripReadOnly)

        //Urlrewrite
        log.debug("Adding {} groups to urlrewrite domain", branchModel.get("displayName"))
        List<String> urlrewriteReadOnly = Arrays.asList(branchName + "-urlrewrite-editor")

        Node urlrewriteNode = domainRootNode.getNode("triodos-templatequery-urlrewrite")
        nodePropertyService.addValuesToMultiValuedProperty(urlrewriteNode.getNode("readonly"), "hipposys:groups", urlrewriteReadOnly)

        //Hippo Requests
        log.debug("Adding {} groups to hipporequests domain", branchModel.get("displayName"))
        List<String> hippoRequestsAuthor = Arrays.asList(branchName + "-kvm-create", branchName + "-bulk-author",
            branchName + "-strip-author")
        List<String> hippoRequestsEditor = Arrays.asList(branchName + "-kvm-change",
            branchName + "-kvm-approve", branchName + "-kvm-takeoffline", branchName + "-bulk-editor",
            branchName + "-strip-editor", branchName + "-channelmanager-editor",
            branchName + "-channelsettings-editor", branchName + "-urlrewrite-editor")
        List<String> hippoRequestsKeyValuePairApprove = Arrays.asList(branchName + "-kvm-approve")

        Node hippoRequestsNode = domainRootNode.getNode("hipporequests")
        nodePropertyService.addValuesToMultiValuedProperty(hippoRequestsNode.getNode("author"), "hipposys:groups", hippoRequestsAuthor)
        nodePropertyService.addValuesToMultiValuedProperty(hippoRequestsNode.getNode("editor"), "hipposys:groups", hippoRequestsEditor)
        nodePropertyService.addValuesToMultiValuedProperty(hippoRequestsNode.getNode("keyvaluepair-approve"), "hipposys:groups", hippoRequestsKeyValuePairApprove)

        //Hst config
        log.debug("Adding {} groups to hstConfig domain", branchModel.get("displayName"))
        List<String> hstConfigReadWrite = Arrays.asList(branchName + "-channelmanager-editor", branchName + "-channelsettings-editor")

        Node hstConfigNode = domainRootNode.getNode("hstconfig")
        nodePropertyService.addValuesToMultiValuedProperty(hstConfigNode.getNode("readwrite"), "hipposys:groups", hstConfigReadWrite)

        //Wpm config
        log.debug("Adding {} groups to hippoWpm domain", branchModel.get("displayName"))
        List<String> hippoWpmReadWrite = Arrays.asList(branchName + "-channelmanager-editor", branchName + "-channelsettings-editor")

        Node hippoWpmNode = domainRootNode.getNode("hippowpm")
        nodePropertyService.addValuesToMultiValuedProperty(hippoWpmNode.getNode("readwrite"), "hipposys:groups", hippoWpmReadWrite)
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
