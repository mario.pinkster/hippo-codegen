package com.triodos.cms.admin.updater

import com.triodos.cms.services.FreemarkerService
import com.triodos.cms.services.hippo.HippoYamlImportService
import org.apache.commons.lang3.StringUtils
import org.hippoecm.hst.configuration.HstNodeTypes
import org.springframework.beans.factory.annotation.Autowired

import javax.jcr.Node
import javax.jcr.NodeIterator

class CreateBranchesUpdater extends BaseNodeUpdater {

    @Autowired protected FreemarkerService freemarkerService;
    @Autowired protected HippoYamlImportService hippoYamlImportService;

    boolean doUpdate(Node jcrRootNode) {
        List<Map<String, Object>> branches = (List<Map<String, Object>>) parametersMap.get("branches")

        Node contentRootNode = jcrRootNode.getNode("content")
        Node hstConfigurationsNode = jcrRootNode.getNode("hst:hst/hst:configurations")
        Node hstSitesNode = jcrRootNode.getNode("hst:hst/hst:sites")
        Node hstHostsNode = jcrRootNode.getNode("hst:hst/hst:hosts")
        Node dashboardShortcutsNode = jcrRootNode.getNode("hippo:configuration/hippo:frontend/cms/cms-dashshortcuts")
        Node taxonomyLeftEditorTemplateNode = jcrRootNode.getNode("hippo:namespaces/hippotaxonomy/taxonomy/editor:templates/_default_/left")
        Node taxonomyTranslationNode = jcrRootNode.getNode("hippo:configuration/hippo:translations/hippo:cms/categoryinfo/en")

        for (Map<String, Object> branch : branches) {
            log.debug("Adding branch {}", branch.get("displayName"))



            if(branch.containsKey("languages")) {
                hstConfigurationsNode.addNode(branch.get("hstAlias"), "hst:configuration")
                    .setProperty(HstNodeTypes.GENERAL_PROPERTY_INHERITS_FROM, ["../root"])
                createSiteNode(hstSitesNode, branch, branch.get("hstAlias"))
                for(Map<String, String> language : branch.get("languages")) {
                    createHstConfiguration(hstConfigurationsNode, language)
                    createWorkspace(hstConfigurationsNode, language)
                    createSiteNode(hstSitesNode, branch, language.get("hstAlias"))
                }
            } else {
                createHstConfiguration(hstConfigurationsNode, branch)
                createWorkspace(hstConfigurationsNode, branch)
                createSiteNode(hstSitesNode, branch, branch.get("hstAlias"))
            }

            createContentFolders(contentRootNode, branch)
            createVirtualHostNodes(hstHostsNode, branch)
            createDashboardShortcuts(dashboardShortcutsNode, branch)
            updateTaxonomy(taxonomyLeftEditorTemplateNode, taxonomyTranslationNode, branch)
        }
        return true
    }

    private void createContentFolders(Node contentRootNode, Map<String, Object> branchModel) {
        log.debug("Adding {} documents folder", branchModel.get("displayName"))
        branchModel.put("translationIds", getTranslationIds(15))
        String branchDocumentsFolderYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-documents-folder.ftl", branchModel)
        Node documentsRootNode = contentRootNode.getNode("documents")
        hippoYamlImportService.importYamlAsString(documentsRootNode, branchDocumentsFolderYaml)

        log.debug("Adding {} gallery", branchModel.get("displayName"))
        String branchGalleryYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-gallery-folder.ftl", branchModel)
        Node galleryRootNode = contentRootNode.getNode("gallery")
        hippoYamlImportService.importYamlAsString(galleryRootNode, branchGalleryYaml)

        log.debug("Adding {} asset folder", branchModel.get("displayName"))
        String branchAssetYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-asset-folder.ftl", branchModel)
        Node assetRootNode = contentRootNode.getNode("assets")
        hippoYamlImportService.importYamlAsString(assetRootNode, branchAssetYaml)

        log.debug("Adding {} urlrewriter folder", branchModel.get("displayName"))
        String branchUrlRewriterYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-urlrewriter-folder.ftl", branchModel)
        Node urlRewriterRootNode = contentRootNode.getNode("urlrewriter")
        hippoYamlImportService.importYamlAsString(urlRewriterRootNode, branchUrlRewriterYaml)
    }

    private static List<String> getTranslationIds(int amount){
        List<String> translationIds = new ArrayList<>(amount)
        for(int i=0; i < amount ; i++){
            translationIds.add(UUID.randomUUID() as String)
        }
        return translationIds
    }

    private void createHstConfiguration(Node hstConfigurationsNode, Map<String, Object> model) {
        log.debug("Adding {} hst configuration", model.get("displayName"))
        String branchHstConfigurationYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-hst-configuration.ftl", model)
        hippoYamlImportService.importYamlAsString(hstConfigurationsNode, branchHstConfigurationYaml)
    }

    private void createWorkspace(Node hstConfigurationsNode, Map<String, Object> model) {
        log.debug("Adding {} workspace", model.get("displayName"))
        Node branchConfigurationNode = hstConfigurationsNode.getNode((String) model.get("hstAlias"))
        String branchHstConfigurationYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-workspace.ftl", model)
        hippoYamlImportService.importYamlAsString(branchConfigurationNode, branchHstConfigurationYaml)
    }

    private void createSiteNode(Node hstSitesNode, Map<String, Object> branchModel, String hstAlias) {
        log.debug("Adding hst:sites node for {}.", (String) branchModel.get("displayName"))
        Node siteNode = hstSitesNode.addNode(hstAlias, "hst:site")
        siteNode.setProperty("hst:content", "/content/documents/" + branchModel.get("branchName"))
    }

    private void createVirtualHostNodes(Node hstHostsNode, Map<String, Object> branchModel) {
        NodeIterator environmentNodeIterator = hstHostsNode.getNodes()
        while (environmentNodeIterator.hasNext()) {
            Node virtualHostGroupNode = environmentNodeIterator.nextNode()

            if (!virtualHostGroupNode.hasNode("eu/triodos")) {
                log.debug("Skipping production host config at {}", virtualHostGroupNode.getPath())
                continue
            }

            log.debug("Adding virtualhost node to {}", virtualHostGroupNode.getPath())
            Node triodosNode = virtualHostGroupNode.getNode("eu/triodos")

            String virtualHostNodeName = String.format("%s-%s", virtualHostGroupNode.getName().replace("-eu", StringUtils.EMPTY), branchModel.get("hstAlias"))
            branchModel.put("virtualHostNodeName", virtualHostNodeName)

            String branchHostYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-hosts.ftl", branchModel)
            hippoYamlImportService.importYamlAsString(triodosNode, branchHostYaml)
        }
    }

    private void createDashboardShortcuts(Node dashboardShortcutsNode, Map<String, Object> branchModel) {
        log.debug("Adding {} dashboard shortcuts", branchModel.get("displayName"))
        String articleShortcutYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-dashboard-shortcut-article.ftl", branchModel)
        hippoYamlImportService.importYamlAsString(dashboardShortcutsNode, articleShortcutYaml)

        String pressReleaseShortcutYaml = freemarkerService.parseTemplateWithModel("templates/yaml/branch-dashboard-shortcut-press-release.ftl",
            branchModel)
        hippoYamlImportService.importYamlAsString(dashboardShortcutsNode, pressReleaseShortcutYaml)
    }

    private void updateTaxonomy(Node taxonomyLeftEditorTemplateNode, Node taxonomyTranslationNode, Map<String, Object> branchModel) {
        log.debug("Adding {} to taxonomy editor template", branchModel.get("displayName"))
        String branchName = branchModel.get("branchName")

        if(nodePropertyService.getIndexOfValue(taxonomyLeftEditorTemplateNode, "branches", branchName) == -1) {
            nodePropertyService.addValueToMultiValuedProperty(taxonomyLeftEditorTemplateNode, "branches", branchName)
        }

        if (!taxonomyTranslationNode.hasProperty(branchName)) {
            taxonomyTranslationNode.setProperty(branchName, (String) branchModel.get("displayName"))
        }
    }

    boolean undoUpdate(Node node) {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
