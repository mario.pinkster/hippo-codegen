package com.triodos.cms.admin.updater

import com.triodos.cms.admin.updater.generator.ChannelGenerator

import javax.jcr.Node
import javax.jcr.RepositoryException

class CreateChannelUpdater extends ChannelGenerator {

    @Override
    boolean doUpdate(Node jcrRootNode) throws RepositoryException {
        return super.doUpdate(jcrRootNode)
    }

    @Override
    boolean undoUpdate(Node node) throws RepositoryException, UnsupportedOperationException {
        throw new UnsupportedOperationException('Updater does not implement undoUpdate method')
    }
}
